//
//  AppConstants.swift
//  Cabbies
//
//  Created by Techwin Labs on 03/04/19.
//  Copyright © 2019 Techwin Labs. All rights reserved.
//
//
import Foundation
import UIKit

let KDone                                           =         "Done"
let KChooseImage                                    =         "Choose Image"
let KChooseVideo                                    =         "Choose Video"
let KCamera                                         =         "Camera"
let KGallery                                        =         "Gallery"
let KYoudonthavecamera                              =         "You don't have camera"
let KSettings                                       =         "Settings"


//MARK: RAZORPAY KEYS

let RAZORPAY_KEY_ID = "rzp_test_oCGcDMHUvhIopO"
let RAZORPAY_KEY_SECRET = "h66PjdxgCVhxhbeNxWahYUmf"



@available(iOS 13.0, *)
let KappDelegate                                    =        UIApplication.shared.delegate as! AppDelegate
let KOpenSettingForPhotos                           =         "App does not have access to your photos. To enable access, tap Settings and turn on Photos."
let KOpenSettingForCamera                           =         "App does not have access to your camera. To enable access, tap Settings and turn on Camera."
let KOK                                             =         "OK"
let KCancel                                         =       "Cancel"
let KYes                                              =       "Yes"
let KNo                                         =       "No"

let KOngoing                                         =       "Ongoing"
let KCompleted                                         =       "Completed"

let GoogleAPIKey = "AIzaSyC9XlPw-l_lY4ga__R5daHFQ8Aj4c8gqOU"


//MARK:- iDevice detection code
struct Device_type
{
    static let IS_IPAD             = UIDevice.current.userInterfaceIdiom == .pad
    static let IS_IPHONE           = UIDevice.current.userInterfaceIdiom == .phone
    static let IS_RETINA           = UIScreen.main.scale >= 2.0
    
    static let SCREEN_WIDTH        = Int(UIScreen.main.bounds.size.width)
    static let SCREEN_HEIGHT       = Int(UIScreen.main.bounds.size.height)
    static let SCREEN_MAX_LENGTH   = Int( max(SCREEN_WIDTH, SCREEN_HEIGHT) )
    static let SCREEN_MIN_LENGTH   = Int( min(SCREEN_WIDTH, SCREEN_HEIGHT) )
    
    static let IS_IPHONE_4_OR_LESS = IS_IPHONE && SCREEN_MAX_LENGTH  < 568
    static let IS_IPHONE_5         = IS_IPHONE && SCREEN_MAX_LENGTH == 568
    static let IS_IPHONE_6         = IS_IPHONE && SCREEN_MAX_LENGTH == 667
    static let IS_IPHONE_6P        = IS_IPHONE && SCREEN_MAX_LENGTH == 736
    static let IS_IPHONE_X         = IS_IPHONE && SCREEN_MAX_LENGTH >= 812
}

//MARK : STATIC TEXT
let kAppName = "Tutor App"

let kLoading = ""
let kVerifying = ""
let kLoading_Getting_OTP = ""

//let kLoading = "Loading..."
//let kVerifying = "Verifying..."
//let kLoading_Getting_OTP = "Requesting OTP..."

let kDone = "Done"
let kSaved = "Saved"
let kError = "Error!"
let kFailed = "Failed!"
let kOops = "Oops!"
let kDataNotFound = "Data not found!"
let kDataNothingTOSHOW = "Sorry nothing to show!"
let kStoryBoard_Main = "Main"
let kResponseNotCorrect = "Data isn't in correct form!"
let kUserNotRegistered = "User is not registered yet!"
let kSomthingWrong = "Something went wrong, please try again!"
let kDataSavedInDatabase = "Data saved successfully in database"
let kDatabaseSuccess = "Database Success"
let kDatabaseFailure = "Database Failure"
let kDrift_ClientID = "LKPHJGMiuHYoUA9rs0qOWLiZf2MLXINw"
let kDrift_ClientSecret = "TWGqKCbMSHvqhoo62SokyIBN3mZC2Irc"
let kDrift_ClientToken = "dvaacsihrwad"


var kTermsConditions = ""
var kPrivacy = ""
var kAboutUs = ""

let kLottieEmpData = "empData"
let kLottieSearch = "searcher"


//MARK: DEFAULT IMAGES
let kplaceholderProfile = "dummy_user"
let kplaceholderImage = "bgFood"//defaultImage
let kplaceholderFood = "bgFood"


let kPush_Approach_from_ForgotPassword = "coming_from_forgotPassword"
let kPush_Approach_from_SignUp = "coming_from_signup"


let mainID = "b21a7c8f-078f-4323-b914-8f59054c4467"//25cbf58b-46ba-4ba2-b25d-8f8f653e9f13

//MARK : KEYS FOR STORE DATA

struct defaultKeys
{
    static let serviceType = "serviceType"
    static let userID = "userID"
    static let userName = "userName"
    static let userFirstName = "userFirstName"
    static let userImage = "userImage"
    static let userDOB = "userDOB"
    static let deliveryType = "deliveryType"
    static let userEmail = "userEmail"
    static let userDeviceToken = "userDeviceToken"
    static let userJWT_Token = "userJWT_Token"
    static let userPhoneNumber = "userPhoneNumber"
    
    static let userHomeAddress = "userHomeAddress"
    static let userAddressType = "userAddressType"
    static let userAddressID = "userAddressID"
    static let userAddressAdded = "userAddressAdded"
    
    
    
    static let firebaseVID = "firebaseVID"
    static let userTYPE = "userTYPE"
    static let userCountryCode = "userCountryCode"
    static let firebaseToken = "firebaseToken"
    static let userLastName = "userLastName"
}

struct database
{
    
    struct entityJobDetails
    {
        
    }
    
    struct entityJobSavedLocations
    {
        
    }
    
}



struct APIAddress
{
    static let BASE_URL = "https://www.k12superheroes.com:8089/api/v1/"
//    https://k12superheros.com:8089/api/v1/login
//    https://conference.infinitywebtechnologies.com:8089/api/v1/
    static let LOGIN = BASE_URL + "login"
    static let STUDENT_REGISTRATION = BASE_URL + "sRegister"
    static let MENTOR_REGISTRATION = BASE_URL + "tRegister"
    static let planListApi = BASE_URL + "getPlanlist"
    static let teacherFacultyDesig = BASE_URL + "tRegisterList"
    static let teachetTookenHistory = BASE_URL + "tTokenHistory/"
    static let tNotificationList = BASE_URL + "getRequestlist/"
    static let tAccetRejectRequest = BASE_URL + "changeStatus"
    static let teacherSignup = BASE_URL + "tSignup"
    static let feedbackToStudent = BASE_URL + "feedbackToStudent"
    
    static let LOGOUT = BASE_URL + "api/mobile/auth/logout"
     static let TokenHistory = BASE_URL + "sTokenHistory/"
    static let Active_Class = BASE_URL + "sBookingList/"
    static let History_Class = BASE_URL + "getCompleteClass/"
     static let GetProfile = BASE_URL + "getProfile/"
    static let SubjecList = BASE_URL + "streamOptions"
      static let TimeSlot = BASE_URL + "getAllSlots"
     static let AddBooking = BASE_URL + "addMultipleBooking"
     static let ScheduleList = BASE_URL + "getAllSchedule/"
    static let TokenAdd = BASE_URL + "addToken"
    static let TeacherActiveList =  BASE_URL + "listBooking/"
     static let TeacherHistoryList =  BASE_URL + "tCompleteClass/"
    static let StartVideoCall =  BASE_URL + "startPrivateClass"
    static let CompleteVideoCall =  BASE_URL + "completeClass"
    static let DeviceToken =  BASE_URL + "updateDeviceToken"
    static let AddSchedulel = BASE_URL + "addSchudle"
    static let SchedulelDetail = BASE_URL + "getScheduleDetails"
    static let UpdateProfile = BASE_URL + "supdateprofile"
    static let cancelBooking = BASE_URL + "cancelBooking"
}

let kHeader_app_json = ["Accept" : "application/json"]


enum Parameter_Keys_All : String
{
    
    case deviceToken = "deviceToken"
    case deviceType = "deviceType"
    case voipDeviceToken = "voipDeviceToken"
    case appVersion = "appVersion"
    
    //User LoginProcess Keys signUp keys
    case language = "language"
    case countryCode = "countryCode"
    case phoneNumber = "phoneNumber"
    case otp = "otp"
    case email = "email"
    case signupBy = "signupBy"
    case firstName = "firstName"
    case lastName = "lastName"
    case socialId = "socialId"
    case loginBy = "loginBy"
    
    case password = "password"
    case address = "address"
    case city = "city"
    case country = "country"
    case latitude = "latitude"
    case longitude = "longitude"
    case socialPic = "socialPic"
    case profilePic = "profilePic"
    case emailPhone = "emailPhone"
    case DOB = "DOB"
    case gender = "gender"
    
}

enum Validate : String
{
    
    case none
    case success = "200"
    case failure = "400"
    case invalidAccessToken = "401"
    case fbLogin = "3"
    case fbLoginError = "405"
    
    func map(response message : String?) -> String?
    {
        
        switch self
        {
        case .success : return message
        case .failure :return message
        case .invalidAccessToken :return message
        case .fbLoginError : return Validate.fbLoginError.rawValue
        default :return nil
        }
    }
}



enum configs
{
    static let mainscreen = UIScreen.main.bounds
    static let kAppdelegate = UIApplication.shared.delegate as! AppDelegate
}


struct AlertTitles
{
    static let Ok:String = "OK"
    static let Cancel:String = "CANCEL"
    static let Yes:String = "Yes"
    static let No:String = "No"
    static let Alert:String = "Alert"
    
    static let Internet_not_available = "Please check your internet connection"
    static let Success = "Success"
    static let Error = "Error"
    static let InternalError = "Internal Error"
    static let Enter_UserName = "Please enter username"
    static let Enter_Email = "Please enter email"
    static let Enter_Password = "Please enter password"
    static let Phone_digits_exceeded = "Phone number digists are exceeded, make sure you are entering correct phone number"
    static let Enter_phone_number = "Please enter phone number"
    static let EnterValid_phone_number = "Please enter a valid phone number"
    static let PasswordEmpty = "Password is empty"
    static let EnterNewPassword = "Please enter new password"
    static let PasswordEmpty_OLD = "Old Password is empty"
    static let PasswordLength8 = "Password length should be of 8-20 characters"
    static let Password_ShudHave_SpclCharacter = "Your password should contain one numeric,one special character,one upper and lower case character"
    static let PasswordCnfrmEmpty = "Confirm password is empty"
    static let Passwordmismatch = "New password and confirm password does not match"
    
    
    
    
}


enum DateFormat
{
    
    case dd_MMMM_yyyy
    case dd_MM_yyyy
    case dd_MM_yyyy2
    case yyyy_MM_dd
    case hh_mm_a
    case yyyy_MM_dd_hh_mm_a
    case yyyy_MM_dd_hh_mm_a2
    case dateWithTimeZone
    case dd_MMM_yyyy
    case yyyy_mm_dd
    
    func get() -> String
    {
        
        switch self
        {
            
        case .dd_MMMM_yyyy : return "dd MMMM, yyyy"
        case .dd_MM_yyyy : return "dd-MM-yyyy"
        case .dd_MM_yyyy2 : return "dd/MM/yyyy"
        case .yyyy_MM_dd : return "yyyy-MM-dd"
        case .hh_mm_a : return "hh:mm a"
        case .yyyy_MM_dd_hh_mm_a : return  "yyyy-MM-dd hh:mm a"
        case .yyyy_MM_dd_hh_mm_a2 : return  "dd MMM yyyy, hh:mm a"
        case .dateWithTimeZone : return "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        case .dd_MMM_yyyy : return "dd MMM yyyy"
        case .yyyy_mm_dd : return "yyyy-mm-dd"
            
        }
    }
}

extension String
{
    func capitalizingFirstLetter() -> String
    {
        return prefix(1).capitalized + dropFirst()
    }
    
    mutating func capitalizeFirstLetter()
    {
        self = self.capitalizingFirstLetter()
    }
}

