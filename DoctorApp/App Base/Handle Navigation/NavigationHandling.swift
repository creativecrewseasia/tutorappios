//
//  Enums.swift
//  UnionGoods
//
//  Created by Rakesh Kumar on 11/22/19.
//  Copyright © 2019 Seasia infotech. All rights reserved.
//

import Foundation
import UIKit

class Navigation
{
    enum type
    {
        case root
        case push
        case present
        case pop
    }
    
    enum Controller
    {
        //AUTH
        case LoginVC
        case SignupVC
        case AddTokenListVC
        case NotificationRequestVC
        case VideoChatViewController
        case RoleVC
        case TeacherSignupVC
     
        
        //MENTOR
        case MentorDashboardVC
        case AudienceViewController
        
        
        
        var obj: UIViewController?
        {
            switch self
            {
                
            //AUTH
            case .LoginVC:
                return StoryBoards.Main.obj?.instantiateViewController(withIdentifier: "LoginVC")
                
            case .SignupVC:
                return StoryBoards.Main.obj?.instantiateViewController(withIdentifier: "SignupVC")
                
            case .RoleVC:
                return StoryBoards.Main.obj?.instantiateViewController(withIdentifier: "RoleVC")
                
            case .TeacherSignupVC:
                return StoryBoards.Main.obj?.instantiateViewController(withIdentifier: "TeacherSignupVC")
                
            case .AddTokenListVC:
                return StoryBoards.Main.obj?.instantiateViewController(withIdentifier: "AddTokenListVC")
                
            case .VideoChatViewController:
                return StoryBoards.Main.obj?.instantiateViewController(withIdentifier: "VideoChatViewController")
            case .NotificationRequestVC:
                return StoryBoards.Main.obj?.instantiateViewController(withIdentifier: "NotificationRequestVC")
                
            //MENTOR
            case .MentorDashboardVC:
                return StoryBoards.Main.obj?.instantiateViewController(withIdentifier: "MentorDashboardVC")
                
            case .AudienceViewController:
                return StoryBoards.Main.obj?.instantiateViewController(withIdentifier: "AudienceViewController")
           
            }
        }
    }
    enum StoryBoards
    {
        case Main
        
        
        var obj: UIStoryboard?
        {
            switch self
            {
            case .Main:
                return UIStoryboard(name: "Main", bundle: nil)
            }
        }
        
    }
    
    static func GetInstance(of controller : Controller) -> UIViewController
    {
        return controller.obj!
    }
    
}

