//
//  CustomViewController.swift
//  GoodsDelivery
//
//  Created by Rakesh Kumar on 12/6/19.
//  Copyright © 2019 Seasia infotech. All rights reserved.
//


import Foundation
import UIKit
import AVFoundation
import Photos
import MobileCoreServices
import NVActivityIndicatorView
import CoreLocation

//MARK:- UI Picker Delegate

protocol UIPickerDelegate{
    func didSelectPicker(index : Int)
    func GetTitleForRow(index:Int) -> String
    func SelectedRow(index:Int)
}

//MARK:- UIImage Picker Delegate
protocol UIImagePickerDelegate
{
    func SelectedMedia(image:UIImage?,imageURL:URL?,videoURL:URL? )
    func selectedImageUrl(url: URL)
    func cancelSelectionOfImg()
}
////MARK:- UIDate Picker Delegate
//@objc protocol SharedUIDatePickerDelegate:class{
//    func doneButtonClicked(datePicker: UIDatePicker)
//    @objc optional func cancelButtonClicked()
//}

class CustomController: UIViewController
{
    private var pickerView = UIPickerView()
    var txtFldForPicker:UITextField?
    var delegate:UIPickerDelegate?
    private var pickerCount:Int?
    var imageURL:URL?
     
    private var imagePicker = UIImagePickerController()
    private var imagePickerDelegate:UIImagePickerDelegate?
    
     private var datePickerView:UIDatePicker!
     private var viewDatePickerView:UIView!
       var pickerType : UIDatePicker.Mode?
      var visualBlurView = UIVisualEffectView()
     private var datePickerDelegate:SharedUIDatePickerDelegate?
    override func viewDidLoad() {
        addShadowNavColor()
    }
    //Show Date Picker for DOB
       func showDatePicker(datePickerDelegate: SharedUIDatePickerDelegate){
           view.endEditing(true)
           
           createBlurEffectView()
           
           self.datePickerDelegate = datePickerDelegate
           if(pickerType == .date){
               self.datePickerView.setDate(Date(), animated: true)
           }else{
               self.datePickerView.setDate(Date(), animated: true)
           }
           self.viewDatePickerView.isHidden = false
        self.datePickerView.maximumDate = Calendar.current.date(byAdding: .year, value: 1, to: Date())
           self.view.insertSubview(viewDatePickerView, aboveSubview: visualBlurView)
       }
    func createBlurEffectView(){
        let blurEffect = UIBlurEffect(style: .dark)
        visualBlurView.effect = blurEffect
        visualBlurView.alpha = 0.3
        visualBlurView.frame = view.bounds
        //        visualBlurView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        //blurview.isHidden = true
        view.addSubview(visualBlurView)
        
    }
    //MARK:- Set UIDatePicker
       func setDatePickerView(_ view: UIView,type: UIDatePicker.Mode){
           view.endEditing(true)
           //UIView for date picker view
           self.viewDatePickerView = UIView(frame: CGRect(x: 0, y: view.frame.size.height - 250, width: view.frame.size.width, height: 244))
           // UIDatePickerView
           self.datePickerView = UIDatePicker(frame: CGRect(x: 0, y: 44, width: viewDatePickerView.frame.size.width, height: 200))
           self.datePickerView.backgroundColor = UIColor.white
           pickerType = type
           if (type == .time){
               self.datePickerView.datePickerMode = .time
           }else{
               self.datePickerView.datePickerMode = .date

               
            
                   self.datePickerView.maximumDate = Calendar.current.date(byAdding: .year, value: 0, to: Date())

           

           }
           // ToolBar for done and cancel
           let toolBar = UIToolbar.init(frame: CGRect(x: 0, y: 0, width: datePickerView.frame.width, height: 44))
           toolBar.barStyle = .default
           toolBar.isTranslucent = true
           toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
           toolBar.sizeToFit()
           
           // Adding Button ToolBar
           let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.clickOnDoneBtn(sender:)))
           let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
           let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.clickOnCancelButton(sender:)))
           
           toolBar.setItems([cancelButton, spaceButton, doneButton], animated: true)
           self.viewDatePickerView.addSubview(toolBar)
           self.viewDatePickerView.addSubview(datePickerView)
           view.addSubview(self.viewDatePickerView)
           self.viewDatePickerView.isHidden = true
           
       }
    //Click on done button for uidatepicker
      @objc func clickOnDoneBtn(sender: Any){
          self.viewDatePickerView.isHidden = true
          datePickerDelegate?.doneButtonClicked(datePicker: datePickerView)
          removeBlurEffect()
      }
      
      //Click on cancel button for uidatepicker
      @objc func clickOnCancelButton(sender: Any){
          self.viewDatePickerView.isHidden = true
          removeBlurEffect()
      }
    
   func removeBlurEffect() {
         visualBlurView.removeFromSuperview()
     }
    
    func addImageOptions()
    {
        DispatchQueue.main.async{
            self.view.endEditing(true)
            let actionSheet1 = UIAlertController(title: "Choose photo" , message: nil, preferredStyle: .actionSheet)
            
            actionSheet1.addAction(UIAlertAction(title: "Camera", style: .default, handler: {(action: UIAlertAction) -> Void in
                // take photo button tapped.
                self.takeNewPhotoFromCamera()
            }))
            actionSheet1.addAction(UIAlertAction(title: "Gallery", style: .default, handler: {(action: UIAlertAction) -> Void in
                // choose photo button tapped.
                self.choosePhotoFromExistingImages()
            }))
            actionSheet1.addAction(UIAlertAction(title:"Cancel", style: .cancel, handler: nil))
            
            self.present(actionSheet1, animated: true, completion: nil)
        }
    }
    //Select pic from gallery
      func choosePhotoFromExistingImages()
      {
          let status = PHPhotoLibrary.authorizationStatus()
          
          if (status == PHAuthorizationStatus.authorized) {
              // Access has been granted.
              if UIImagePickerController.isSourceTypeAvailable(.photoLibrary)
              {
                  let controller = UIImagePickerController()
                  controller.sourceType = .savedPhotosAlbum
                  controller.allowsEditing = false
                  controller.delegate = self as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
                  DispatchQueue.main.async {
                      self.navigationController!.present(controller, animated: true, completion: nil)
                  }
              }
          }
              
          else if (status == PHAuthorizationStatus.denied) {
              // Access has been denied.
              let alert = UIAlertController(title: "Alert", message: "Open setting to enable camera permission.", preferredStyle: .alert)
              
              alert.addAction(UIAlertAction(title: "Open Setting", style: .default, handler: { (_) in
                  DispatchQueue.main.async {
                      if let settingsURL = URL(string: UIApplication.openSettingsURLString) {
                          UIApplication.shared.open(settingsURL, options: [:], completionHandler: nil)
                      }
                  }
              }))
              alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
              self.present(alert, animated: true, completion: nil)
          }
              
          else if (status == PHAuthorizationStatus.notDetermined)
          {
              
              // Access has not been determined.
              PHPhotoLibrary.requestAuthorization({ (newStatus) in
                  
                  if (newStatus == PHAuthorizationStatus.authorized) {
                      if UIImagePickerController.isSourceTypeAvailable(.photoLibrary)
                      {
                          let controller = UIImagePickerController()
                          controller.sourceType = .savedPhotosAlbum
                          controller.allowsEditing = false
                          controller.delegate = self as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
                          DispatchQueue.main.async {
                              self.navigationController!.present(controller, animated: true, completion: nil)
                          }
                      }
                  }
                  else
                  {
                      
                  }
              })
          }
              
          else if (status == PHAuthorizationStatus.restricted)
          {
              // Restricted access - normally won't happen.
          }
      }
    func takeNewPhotoFromCamera()
    {
        view.endEditing(true)
        AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { (granted :Bool) -> Void in
            if granted == true
            {
                
                
                
                if UIImagePickerController.isSourceTypeAvailable(.camera)
                {
                    let controller = UIImagePickerController()
                    controller.sourceType = .camera
                    controller.allowsEditing = false
                    controller.delegate = self as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
                    DispatchQueue.main.async {
                        self.navigationController?.present(controller, animated: true, completion: nil)
                    }
                    
                }
                
            }
            else
            {
                let alert = UIAlertController(title: "Alert", message: "Open setting to enable camera permission.", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "OpenSettings", style: .default, handler: { (_) in
                    DispatchQueue.main.async {
                        if let settingsURL = URL(string: UIApplication.openSettingsURLString) {
                            UIApplication.shared.open(settingsURL, options: [:], completionHandler: { (success) in
                                print(success)
                            })
                        }
                    }
                }))
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
                return
            }
        })
        
    }
    //MARK:-Tap gesture for swrevealcontroller
    func setTapGestureOnSWRevealontroller(view: UIView,controller: UIViewController)
    {
        view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        view.frame = CGRect(x: UIScreen.main.bounds.origin.x, y: self.view.frame.origin.y, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.height)
        let revealController: SWRevealViewController? = revealViewController()
        let tap: UITapGestureRecognizer? = revealController?.tapGestureRecognizer()
        tap?.delegate = controller as? UIGestureRecognizerDelegate
        self.revealViewController().panGestureRecognizer().isEnabled = false
        self.revealViewController().tapGestureRecognizer().isEnabled = true
        view.addGestureRecognizer(tap!)
        
    }
    
    func addShadowNavColor()
    {
        
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.black.cgColor
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0.0, height: 0.5)
        self.navigationController?.navigationBar.layer.shadowRadius = 2.0
        self.navigationController?.navigationBar.layer.shadowOpacity = 1.0
        self.navigationController?.navigationBar.layer.masksToBounds = false
        
    }
    
    func addShadowToTextField(textField:UITextField) {

               textField.layer.borderColor = UIColor.black.cgColor
               textField.borderStyle = UITextField.BorderStyle.none
               textField.layer.masksToBounds = false
               textField.layer.cornerRadius = 7.0;
               textField.layer.backgroundColor = UIColor.white.cgColor
              textField.layer.borderColor = UIColor.clear.cgColor
               textField.layer.shadowColor = UIColor.black.cgColor
               textField.layer.shadowOffset = CGSize(width: 0, height: 0)
               textField.layer.shadowOpacity = 0.15
               textField.layer.shadowRadius = 4.0

        
    }
    
    func txtfieldPadding(textField : UITextField){
        
        let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: 45))
        textField.leftView = paddingView
        textField.leftViewMode = .always
        
    }
    
    func  CornerRadius(radius:Int,view:UIView )
    {
        view.layer.cornerRadius = CGFloat(radius)
        view.layer.masksToBounds = true
        view.clipsToBounds = true
    }
    
    func UIPickerDataSurce(txtFld: UITextField, delegate : UIPickerDelegate,count:Int)
    {
        self.delegate = delegate
        txtFldForPicker = txtFld
        pickerCount = count
        AddPicker()
    }
    
    
    //MARK:-OpenGallery and Camera
    func OpenGalleryCamera(camera:Bool,imagePickerDelegate:UIImagePickerDelegate,isVideoAlso:Bool)
    {
        self.imagePickerDelegate = imagePickerDelegate
        let alert = UIAlertController(title: KChooseImage, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: KCamera, style: .default, handler: { _ in
            self.OpenCamera(imagePickerDelegate: imagePickerDelegate, isVideoAlso: isVideoAlso)
        }
        ))
        alert.addAction(UIAlertAction(title: KGallery, style: .default, handler: { _ in
            self.OpenGallary(imagePickerDelegate: imagePickerDelegate, isVideoAlso: isVideoAlso)
        }))
        alert.addAction(UIAlertAction.init(title: KCancel, style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func OpenCamera(imagePickerDelegate:UIImagePickerDelegate, isVideoAlso:Bool)
    {
        self.imagePickerDelegate = imagePickerDelegate
        if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized {
            //  Open Camera
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera)
            {
//                self.imagePicker.delegate = self
                self.imagePicker.sourceType = UIImagePickerController.SourceType.camera
                self.imagePicker.allowsEditing = true
                if(isVideoAlso)
                {
                    //self.imagePicker.mediaTypes = [kUTTypeMovie as String,kUTTypeImage as String ]
                    self.imagePicker.mediaTypes = [kUTTypeImage as String ]
                }
                else
                {
                    self.imagePicker.mediaTypes = [kUTTypeImage as String ]
                }
                // self.imagePicker.mediaTypes = [kUTTypeImage as String]
                self.present(self.imagePicker, animated: true, completion: nil)
            }
            else
            {
                self.showAlertMessage(titleStr: kAppName, messageStr: KYoudonthavecamera)
            }
            
        } else
        {
            // Open setting alert for camera
            let alert = UIAlertController(title: KOpenSettingForCamera , message: "", preferredStyle: UIAlertController.Style.alert)
            
            alert.addAction(UIAlertAction(title: KCancel, style: .default))
            alert.addAction(UIAlertAction(title: KSettings, style: .cancel) { (alert) -> Void in
                UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
            })
            self.present(alert, animated: true)
            ////////
        }
    }
    func OpenGallary(imagePickerDelegate:UIImagePickerDelegate, isVideoAlso:Bool)
    {
        self.imagePickerDelegate = imagePickerDelegate
        let status = PHPhotoLibrary.authorizationStatus()
        switch status {
        case .authorized:
            // Open Gallary
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary) {
//                self.imagePicker.delegate = self
                self.imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
                if(isVideoAlso)
                {
                    // self.imagePicker.mediaTypes = [kUTTypeMovie as String,kUTTypeImage as String ]
                    self.imagePicker.mediaTypes = [kUTTypeImage as String ]
                    
                }
                else
                {
                    self.imagePicker.mediaTypes = [kUTTypeImage as String ]
                }
                self.imagePicker.allowsEditing = true
                self.present(self.imagePicker, animated: true, completion: nil)
                /////////
            }
        case .denied, .restricted:
            // Open setting Alert for galllary
            // Open setting alert for camera
            let alert = UIAlertController(title: KOpenSettingForPhotos , message: "", preferredStyle: UIAlertController.Style.alert)
            
            alert.addAction(UIAlertAction(title: KCancel, style: .default))
            alert.addAction(UIAlertAction(title: KSettings, style: .cancel) { (alert) -> Void in
                UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
            })
            self.present(alert, animated: true)
        //////////
        case .notDetermined:
            break
        @unknown default:
            break
        }
        
    }
    
    func OpenGallaryCameraForVideo(imagePickerDelegate:UIImagePickerDelegate)
    {
        self.imagePickerDelegate = imagePickerDelegate
        let alert = UIAlertController(title: KChooseImage, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: KCamera, style: .default, handler: { _ in
            if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized {
                //  Open Camera
                if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
//                    self.imagePicker.delegate = self
                    self.imagePicker.sourceType = UIImagePickerController.SourceType.camera
                    // pickerController.allowsEditing = true
                    self.imagePicker.videoMaximumDuration = 30
                    self.imagePicker.videoQuality = UIImagePickerController.QualityType.typeMedium
                    self.imagePicker.allowsEditing = false
                    self.imagePicker.mediaTypes = [kUTTypeMovie as String]
                    //   UIImagePickerController.availableMediaTypes(for:.camera)!;
                    self.present(self.imagePicker, animated: true, completion: nil)
                }
                else {
                    self.showAlertMessage(titleStr: kAppName, messageStr: KYoudonthavecamera)
                }
                //////////
            } else
            {
                // Open setting alert for camera
                let alert = UIAlertController(title: KOpenSettingForCamera , message: "", preferredStyle: UIAlertController.Style.alert)
                
                alert.addAction(UIAlertAction(title: KCancel, style: .default))
                alert.addAction(UIAlertAction(title: KSettings, style: .cancel) { (alert) -> Void in
                    UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
                })
                self.present(alert, animated: true)
                ////////
            }
        }))
        alert.addAction(UIAlertAction(title: KGallery, style: .default, handler: { _ in
            let status = PHPhotoLibrary.authorizationStatus()
            switch status {
            case .authorized:
                // Open Gallary
                if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary) {
//                    self.imagePicker.delegate = self
                    self.imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
                    self.imagePicker.allowsEditing = true
                    self.imagePicker.videoMaximumDuration = 30
                    self.imagePicker.videoQuality = UIImagePickerController.QualityType.typeMedium
                    //pickerController.allowsEditing = true
                    self.imagePicker.mediaTypes = [kUTTypeMovie as String]
                    //   UIImagePickerController.availableMediaTypes(for:.camera)!;
                    self.present(self.imagePicker, animated: true, completion: nil)
                }
            case .denied, .restricted:
                // Open setting Alert for galllary
                // Open setting alert for camera
                let alert = UIAlertController(title: KOpenSettingForPhotos , message: "", preferredStyle: UIAlertController.Style.alert)
                
                alert.addAction(UIAlertAction(title: KCancel, style: .default))
                alert.addAction(UIAlertAction(title: KSettings, style: .cancel) { (alert) -> Void in
                    UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
                })
                self.present(alert, animated: true)
            //////////
            case .notDetermined:
                break
            @unknown default: break
               
            }
        }))
        alert.addAction(UIAlertAction.init(title: KCancel, style: .cancel, handler: nil))
        
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    @objc func DonePicker()
    {
        let row = pickerView.selectedRow(inComponent: 0)
        delegate?.didSelectPicker(index: row)
        
        txtFldForPicker?.resignFirstResponder()
    }
    @objc func CancelPicker()
    {
        txtFldForPicker?.resignFirstResponder()
    }
    
    
    
}

//MARK: UIPickerViewDataSource,UIPickerViewDelegate
extension CustomController:UIPickerViewDataSource,UIPickerViewDelegate
{
    func AddPicker()
    {
        
        pickerView.delegate = self
        pickerView.dataSource = self
        txtFldForPicker!.inputView = pickerView
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 3/255, green: 95/255, blue: 253/255, alpha: 1)
        toolBar.sizeToFit()
        let doneButton = UIBarButtonItem(title: KDone, style: UIBarButtonItem.Style.plain, target: self, action: #selector(DonePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: KCancel, style: UIBarButtonItem.Style.plain, target: self, action: #selector(CancelPicker))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        txtFldForPicker!.inputAccessoryView = toolBar
    }
    
    
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return pickerCount ?? 0
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.delegate?.GetTitleForRow(index: row)
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        
        self.delegate?.SelectedRow(index: row)
    }
}
//extension CustomController:UIImagePickerControllerDelegate,UINavigationControllerDelegate
//{
//    func imagePickerControllerDidCancel(_ picker: UIImagePickerController){
//        self.imagePickerDelegate?.cancelSelectionOfImg()
//        self.dismiss(animated: true, completion: nil)
//    }
//    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
//        var isImage:Bool = false
//
//        guard let mediaType = info[UIImagePickerController.InfoKey.mediaType] as? String else {return}
//        print(mediaType)
//        switch mediaType{
//        case "public.image":
//            isImage = true;
//            break;
//        case "public.video":
//            isImage = false;
//            break;
//        default:
//            break;
//        }
//        if(isImage == true){
//
//            var pickedImage : UIImage!
//
//               if let img = info[UIImagePickerController.InfoKey.editedImage] as? UIImage
//               {
//                   pickedImage = img
//
//               }
//               else if let img = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
//               {
//                   pickedImage = img
//               }
//
//                if #available(iOS 11.0, *) {
//                    if pickedImage != nil {
//
//                        //   pickedImage =  pickedImage.fixedOrientation()!
//
//                        var urlImage:URL?
//                        guard let chosenImagee = pickedImage else {
//                            fatalError("\(info)")
//                        }
//                        let chosenImage =  chosenImagee.fixedOrientation()!
//
//                        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
//                        // choose a name for your image
//                        let fileName = "\(Double(Date.timeIntervalSinceReferenceDate * 1000)).jpg"
//                        // create the destination file url to save your image
//                        let fileURL = documentsDirectory.appendingPathComponent(fileName)
//
//                        if let data = chosenImage.jpegData(compressionQuality: 1.0),
//                            !FileManager.default.fileExists(atPath: fileURL.path) {
//                            do {
//                                // writes the image data to disk
//                                try data.write(to: fileURL)
//
//                                let url = fileURL
//                                urlImage = url
//
//                            } catch {
//
//                            }
//                        }
//                        if let url = urlImage{
//                            imageURL = url
//                            imagePickerDelegate?.selectedImageUrl(url: url)
//                        }
//                    }
//                    dismiss(animated: true, completion: nil)
//
//                } else {
//                    //                  Fallback on earlier versions
//                    var urlImage:URL?
//                    guard let chosenImage = pickedImage else {
//                        fatalError("\(info)")
//                    }
//                    let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
//                    // choose a name for your image
//                    let fileName = "\(Double(Date.timeIntervalSinceReferenceDate * 1000)).jpg"
//                    // create the destination file url to save your image
//                    let fileURL = documentsDirectory.appendingPathComponent(fileName)
//
//                    if let data = chosenImage.jpegData(compressionQuality: 1.0),
//                        !FileManager.default.fileExists(atPath: fileURL.path) {
//                        do {
//                            // writes the image data to disk
//                            try data.write(to: fileURL)
//                            let url = fileURL
//                            urlImage = url
//                        } catch {
//                            //       CommonFunctions.sharedmanagerCommon.println(object: "Exception while writing the url image")
//                        }
//                    }
//                    if let url = urlImage{
//                        imageURL = url
//                        imagePickerDelegate?.selectedImageUrl(url: url)
//                    }
//                }
//                print(imageURL!)
//                imagePickerDelegate?.SelectedMedia(image: pickedImage, imageURL: imageURL, videoURL: nil)
//
//            dismiss(animated: true, completion: nil)
//        }
//        else{
//            guard let videoURL = info[UIImagePickerController.InfoKey.mediaURL] as? URL else {return}
//
//            do {
//                let asset = AVURLAsset(url: videoURL , options: nil)
//                let imgGenerator = AVAssetImageGenerator(asset: asset)
//                imgGenerator.appliesPreferredTrackTransform = true
//                let cgImage = try imgGenerator.copyCGImage(at: CMTimeMake(value: 0, timescale: 1), actualTime: nil)
//                let thumbnail = UIImage(cgImage: cgImage)
//                imagePickerDelegate?.SelectedMedia(image: thumbnail,imageURL:nil,videoURL:videoURL )
//            } catch  {
//                //     CommonFunctions.sharedmanagerCommon.println(object: "*** Error generating thumbnail: \(error.localizedDescription)")
//            }
//            self.dismiss(animated: true, completion: nil)
//        }
//    }
//
//    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
//        // When showing the ImagePicker update the status bar and nav bar properties.
//        //UIApplication.shared.setStatusBarHidden(false, with: .none)
//        //164 13 28
//        navigationController.topViewController?.title = "Select photo iSMS"
//        navigationController.navigationBar.isTranslucent = false
//
//        navigationController.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
//        navigationController.navigationBar.barStyle = .default
//        navigationController.setNavigationBarHidden(false, animated: animated)
//    }
//}




extension UIImage {
    
    func fixedOrientation() -> UIImage? {
        
        guard imageOrientation != UIImage.Orientation.up else {
            //This is default orientation, don't need to do anything
            return self.copy() as? UIImage
        }
        
        guard let cgImage = self.cgImage else {
            //CGImage is not available
            return nil
        }
        
        guard let colorSpace = cgImage.colorSpace, let ctx = CGContext(data: nil, width: Int(size.width), height: Int(size.height), bitsPerComponent: cgImage.bitsPerComponent, bytesPerRow: 0, space: colorSpace, bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue) else {
            return nil //Not able to create CGContext
        }
        
        var transform: CGAffineTransform = CGAffineTransform.identity
        
        switch imageOrientation {
        case .down, .downMirrored:
            transform = transform.translatedBy(x: size.width, y: size.height)
            transform = transform.rotated(by: CGFloat.pi)
            break
        case .left, .leftMirrored:
            transform = transform.translatedBy(x: size.width, y: 0)
            transform = transform.rotated(by: CGFloat.pi / 2.0)
            break
        case .right, .rightMirrored:
            transform = transform.translatedBy(x: 0, y: size.height)
            transform = transform.rotated(by: CGFloat.pi / -2.0)
            break
        case .up, .upMirrored:
            break
        @unknown default: break
            
        }
        
        //Flip image one more time if needed to, this is to prevent flipped image
        switch imageOrientation {
        case .upMirrored, .downMirrored:
            transform.translatedBy(x: size.width, y: 0)
            transform.scaledBy(x: -1, y: 1)
            break
        case .leftMirrored, .rightMirrored:
            transform.translatedBy(x: size.height, y: 0)
            transform.scaledBy(x: -1, y: 1)
        case .up, .down, .left, .right:
            break
        @unknown default: break
            
        }
        
        ctx.concatenate(transform)
        
        switch imageOrientation {
        case .left, .leftMirrored, .right, .rightMirrored:
            ctx.draw(self.cgImage!, in: CGRect(x: 0, y: 0, width: size.height, height: size.width))
        default:
            ctx.draw(self.cgImage!, in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
            break
        }
        
        guard let newCGImage = ctx.makeImage() else { return nil }
        return UIImage.init(cgImage: newCGImage, scale: 1, orientation: .up)
    }
}
