//
//  AppID.swift
//  Agora iOS Tutorial
//
//  Created by James Fang on 7/19/16.
//  Copyright © 2016 Agora.io. All rights reserved.
//

let AppID: String = "f989a8e0d53a44b68f1cd9ce017ade0e"

//let AppID: String = "3c6f62a1455642ba8269bf7820693ddf"
/* assign Token to nil if you have not enabled app certificate
 * before you deploy your own token server, you can easily generate a temp token for dev use
 * at https://dashboard.agora.io note the token generated are allowed to join corresponding room ONLY.
 */
/* 如果没有打开鉴权Token, 这里的token值给nil就好
 * 生成Token需要参照官方文档部署Token服务器，开发阶段若想先不部署服务器, 可以在https://dashbaord.agora.io生成
 * 临时Token. 请注意生成Token时指定的频道名, 该Token只允许加入对应的频道
 */
//let Token: String? = "006f989a8e0d53a44b68f1cd9ce017ade0eIADqBw7jogc0fplaZMgDD9PMwtplM91vHcATUJV0ObTso5SrOnoAAAAAEAAebVBBtIU7XwEAAQC0hTtf"//vishav

//let Token: String? = "006f989a8e0d53a44b68f1cd9ce017ade0eIAACoVaee0kCMgi2SDsVQ/nEo+ztQYEmCebn9Rac89227RJtWVYAAAAAEAChLLDAa+88XwEAAQBq7zxf"

let Token = String("006f989a8e0d53a44b68f1cd9ce017ade0eIABC9ms/bpFwmTTUoGH//u3cffmvfx3cS1xjxAGhr3Qj0JKeClIh39v0EABZJmZOwkg+XwEAAQAAAAAA")
