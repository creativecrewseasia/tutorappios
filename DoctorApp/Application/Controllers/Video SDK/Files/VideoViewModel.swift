//
//  VideoViewModel.swift
//  DoctorApp
//
//  Created by Atinder Kaur on 8/27/20.
//  Copyright © 2020 Cerebrum Infotech. All rights reserved.
//

import Foundation
protocol VideoVCDelegate: class {
   
    func finished ()
    
}



class VideoViewModel
{
    var view : VideoChatViewController
    var localVideoVCDelegate : VideoVCDelegate?
  
    init(view : VideoChatViewController, delegate: VideoVCDelegate)
       {
           self.view = view
           localVideoVCDelegate = delegate
       }
    
    
    func endVideoCall(Params : [String:Any]) {
          
                
                WebService.Shared.PostApi(url: APIAddress.CompleteVideoCall, parameter: Params, Target: self.view, showLoader: true, completionResponse: { (response) in
                         Commands.println(object: response)
                         
                                     do
                                     {
                                        let data = response as? [String : Any]
                                        if let code = data?["code"] as? Int  {
                                            if code == 200 {
                                                self.localVideoVCDelegate?.finished()
                                            }
                                        }
                                     }
                                     catch
                                     {
                                         self.view.showToastSwift(alrtType: .error, msg: kResponseNotCorrect, title: kOops)
                                     }
                     }) { (error) in
                                                   self.view.showToastSwift(alrtType: .error, msg: error, title: kOops)

                     }
            
        }
    
    func updateDeviceToken(Params : [String:Any]) {
        WebService.Shared.PostApi(url: APIAddress.DeviceToken, parameter: Params, Target: self.view, showLoader: true, completionResponse: { (response) in
                                Commands.println(object: response)
                                            do
                                            {
                                               let data = response as? [String : Any]
                                               if let code = data?["code"] as? Int  {
                                                   if code == 200 {
                                                   }
                                               }
                                            }
                                            catch
                                            {
                                            }
                            }) { (error) in

                            }
                   
    }

}

