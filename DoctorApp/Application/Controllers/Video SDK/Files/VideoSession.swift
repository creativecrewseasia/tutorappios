//
//  VideoSession.swift
//  DoctorApp
//
//  Created by Poonam  on 14/09/20.
//  Copyright © 2020 Cerebrum Infotech. All rights reserved.
//

import Foundation
import UIKit
import AgoraRtcKit

class VideoSession: NSObject {
    var uid: Int64 = 0
    var hostingView: UIView!
    var canvas: AgoraRtcVideoCanvas!
    
    init(uid: Int64) {
        self.uid = uid
        
        hostingView = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        hostingView.translatesAutoresizingMaskIntoConstraints = false
        
        canvas = AgoraRtcVideoCanvas()
        canvas.uid = UInt(uid)
        canvas.view = hostingView
        canvas.renderMode = .fit
    }
}
