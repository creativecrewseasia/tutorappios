//
//  MentorDashboardVC.swift
//  DoctorApp
//
//  Created by Mohit Sharma on 8/19/20.
//  Copyright © 2020 Cerebrum Infotech. All rights reserved.
//

import UIKit

class MentorDashboardVC: CustomController {

    @IBOutlet weak var customSegment: UISegmentedControl!
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var btnMenu: UIBarButtonItem!
    @IBOutlet weak var tblViewListing: UITableView!
    var viewModel:MentorMentorDBViewModel?
    var dataList : [SubList]?
    var refreshControl = UIRefreshControl()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        setView()
        // Do any additional setup after loading the view.
    }
    
   
    
    func setView() {
        self.viewModel = MentorMentorDBViewModel.init(view: self, delegate: self)
        
        tblViewListing.tableFooterView = UIView()
        tblViewListing.separatorColor = UIColor.clear
        activeData()
        customSegment.selectedSegmentIndex = 0
        if #available(iOS 13.0, *) {
            customSegment.selectedSegmentTintColor = UIColor.white
        } else {
            // Fallback on earlier versions
        }
       
    refreshControl.attributedTitle = NSAttributedString(string: "Loading")
                      refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
               tblViewListing.addSubview(refreshControl)
        if AppDefaults.shared.firebaseToken != "" {
                 var param = [String : Any]()
                 param["deviceToken"] = AppDefaults.shared.firebaseToken
                  param["userId"] = AppDefaults.shared.userID
                  self.viewModel?.updateDeviceToken(Params: param)
               }
           }
           
           @objc func refresh(_ sender: AnyObject) {
                     if AllUtilies.isConnectedToInternet
                            {
                                if (customSegment.selectedSegmentIndex == 0)
                                              {
                                                  self.activeData()
                                              }
                                              else
                                              {
                                                  self.historyData()
                                              }
                    }
                            else
                            {
                                self.showToastSwift(alrtType:.info, msg: "No internet connection.", title:kOops)
                            }
              }
           
    
    func activeData() {
      self.viewModel?.getActiveClasses()
    }
    
    func historyData() {
         self.viewModel?.getHistoryClasses()
    }
    
  //Button Actions
    @IBAction func actionSideMenu(_ sender: UIButton) {
       self.revealViewController()?.revealToggle(animated: true)
    }
   
    @IBAction func actionNotification(_ sender: UIButton) {
        let controller = Navigation.GetInstance(of: .NotificationRequestVC)as! NotificationRequestVC
        self.push_To_Controller(from_controller: self, to_Controller: controller)
    }
    
    @IBAction func actionAdd(_ sender: UIButton) {
        if AllUtilies.isConnectedToInternet
                  {
                     let storyBoard: UIStoryboard = UIStoryboard(name: "Student2", bundle: nil)
                     let newViewController = storyBoard.instantiateViewController(withIdentifier: "AddMeetingVC") as! AddMeetingVC
                          newViewController.modalPresentationStyle = .fullScreen
                     self.navigationController?.pushViewController(newViewController, animated: true)
          }
                  else
                  {
                      self.showToastSwift(alrtType:.info, msg: "No internet connection.", title:kOops)
                  }
         
    }
    
    @IBAction func joinVideoCall(_ sender: UIButton) {
    var param = [String : Any]()
                        param["classId"] = "11bf5b37-e0b8-42e0-8dcf-dc8c4aefc000"
                        param["subjectId"] = "11bf5b37-e0b8-42e0-8dcf-dc8c4aefc000"
                        param["topicId"] = "2ea15426-ee5f-4c3a-bc83-45eca850036d"
                        param["userId"] = AppDefaults.shared.userID
                        param["sectionId"] = "11bf5b37-e0b8-42e0-8dcf-dc8c4aefc000"
                        param["studentId"] = dataList?[sender.tag].userID
                        param["bookingId"] = dataList?[sender.tag].id
        self.viewModel?.startVideoCall(Params: param, tag : sender.tag)
        
    }
    
    @IBAction func valueChangedSegment(_ sender: UISegmentedControl) {
        
        if AllUtilies.isConnectedToInternet
                {
                    if (sender.selectedSegmentIndex == 0)
                                  {
                                      self.activeData()
                                  }
                                  else
                                  {
                                      self.historyData()
                                  }
        }
                else
                {
                    self.showToastSwift(alrtType:.info, msg: "No internet connection.", title:kOops)
                }
       
        
    }
    

}

extension MentorDashboardVC : MentorDBDelegate {
    func addedSuccessfully(result: ListModel) {
        
          self.refreshControl.endRefreshing()
        dataList?.removeAll()
        if result.data?.count ?? 0 > 0 {
            dataList = result.data
          
        }
        
          tblViewListing.reloadData()
    }
    
    func callStarted (tag : Int)
    {
        let controller = Navigation.GetInstance(of: .VideoChatViewController)as! VideoChatViewController
             controller.channelName = dataList?[tag].channelName ?? ""
             controller.accessToken = dataList?[tag].accessToken ?? ""
             self.push_To_Controller(from_controller: self, to_Controller: controller)
    }
    
    func failed() {
        
    }
}


extension MentorDashboardVC : UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if dataList?.count  ?? 0 > 0 {
            return (dataList?.count ?? 0)
        }else{
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! DashboardCell
        cell.lblDate.text = dataList?[indexPath.row].bookingDate
        cell.lblTime.text = dataList?[indexPath.row].timeSlot
        cell.btnJoin.tag = indexPath.row
        
        if  customSegment.selectedSegmentIndex == 1 {
            cell.btnJoin.isHidden = true
        }else{
            if (dataList?[indexPath.row].status ?? 0 == 0){
            cell.btnJoin.isHidden = false
            }
            else{
             cell.btnJoin.isHidden = true
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 98
    }
}
