//
//  AddTokenListViewModel.swift
//  DoctorApp
//
//  Created by Navaldeep Kaur on 25/08/20.
//  Copyright © 2020 Cerebrum Infotech. All rights reserved.
//

import Foundation


protocol NotificationListDelegate:class
{
    func Show(msg: String)
    func didError(error:String)
}

class NotificationnListViewModel
{
    typealias successHandler = (NotificationRequestModel) -> Void
    typealias acceptRejectSuccess = (AcceptRejectModel) -> Void
    
    var delegate : NotificationListDelegate
    var view : UIViewController
    var isSearching : Bool?
    var showLoader:Bool?
    
    init(Delegate : NotificationListDelegate, view : UIViewController)
    {
        delegate = Delegate
        self.view = view
    }
    
    //MARK:- GetNotificationApi
    func getNotificationListApi(completion: @escaping successHandler)
    {
        WebService.Shared.GetApi(url: APIAddress.tNotificationList + AppDefaults.shared.userID, Target: self.view, showLoader: true, completionResponse: { (response) in
            do
            {
                let jsonData = try JSONSerialization.data(withJSONObject: response, options: .prettyPrinted)
                let getAllListResponse = try JSONDecoder().decode(NotificationRequestModel.self, from: jsonData)
                completion(getAllListResponse)
            }
            catch
            {
                self.delegate.didError(error: kResponseNotCorrect)
                self.view.showToastSwift(alrtType: .error, msg: kResponseNotCorrect, title: kOops)
            }
        }) { (error) in
            self.delegate.didError(error: error)
            self.view.showToastSwift(alrtType: .error, msg: error, title: kFailed)
        }
        
    }
    
    func acceptRejectRequestApi(status:Int?,bookingId:String?,teacherId:String,channelName:String?,accessToken:String?,completion: @escaping acceptRejectSuccess)
     {
        var params:[String:Any] = ["status":status ?? "","bookingId" : bookingId ?? "","teacherId":teacherId,"channelName": channelName ?? "","accessToken":accessToken ?? "" ]
         WebService.Shared.PostApi(url: APIAddress.tAccetRejectRequest, parameter: params , Target: self.view, showLoader: true, completionResponse: { response in
             
             Commands.println(object: response)
             
             do
             {
                 let jsonData = try JSONSerialization.data(withJSONObject: response, options: .prettyPrinted)
                 let model = try JSONDecoder().decode(AcceptRejectModel.self, from: jsonData)
              completion(model)
             }
             catch
             {
                 self.view.showToastSwift(alrtType: .error, msg: kResponseNotCorrect, title: kOops)
             }
             
         }, completionnilResponse: {(error) in
             self.view.showToastSwift(alrtType: .error, msg: error, title: kOops)
         })
         
     }
    
}

//MARK:- AddTokenListDelegate
extension NotificationRequestVC : NotificationListDelegate
{
    func Show(msg: String) {
    }
    
    func didError(error: String) {
        
           // orderHistory.removeAll()
            tbleView.setEmptyMessage("Sorry nothing to show!")
       
    }
    
    
}
