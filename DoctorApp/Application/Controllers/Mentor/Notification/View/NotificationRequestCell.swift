//
//  NotificationRequestCell.swift
//  DoctorApp
//
//  Created by Navaldeep Kaur on 26/08/20.
//  Copyright © 2020 Cerebrum Infotech. All rights reserved.
//

import UIKit

class NotificationRequestCell: UITableViewCell {
    @IBOutlet weak var btnAccept: CustomButton!
      @IBOutlet weak var btnReject: CustomButton!
      @IBOutlet weak var lblName: UILabel!
      @IBOutlet weak var lblTime: UILabel!
      @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var kStackHeight: NSLayoutConstraint!
    @IBOutlet weak var kTop: NSLayoutConstraint!
    
    var viewDelegate :NotificationRequestDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func rejectAction(_ sender: UIButton) {
        viewDelegate?.reject(index:sender.tag)
       }
       @IBAction func acceptAction(_ sender: UIButton) {
        viewDelegate?.accept(index:sender.tag)
       }

}
