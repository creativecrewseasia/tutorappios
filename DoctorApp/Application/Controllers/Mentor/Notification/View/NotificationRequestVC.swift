//
//  NotificationRequestVC.swift
//  DoctorApp
//
//  Created by Navaldeep Kaur on 26/08/20.
//  Copyright © 2020 Cerebrum Infotech. All rights reserved.
//

import UIKit

protocol NotificationRequestDelegate:class {
    func accept(index:Int)
    func reject(index:Int?)
}
class NotificationRequestVC: UIViewController {

    //MARK:- Outlet and variables
    
    @IBOutlet weak var tbleView: UITableView!
    var viewModel : NotificationnListViewModel?
    var notificationList = [NotificationDatum]()
    
    //MARK:- life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()

        setView()
    }

  //MARK:- Other function
    func setView(){
        viewModel = NotificationnListViewModel.init(Delegate: self, view: self)
        tbleView.delegate = self
        tbleView.dataSource = self
        tbleView.tableFooterView = UIView()
        tbleView.separatorStyle = .none
        getNotificationList()
    }
    
    
    //MARK:-Apis
    func getNotificationList(){
        viewModel?.getNotificationListApi(completion: { (response) in
        if let data = response.data{
            if data.count > 0{
                self.notificationList = data
                self.tbleView.setEmptyMessage("")
                self.tbleView.reloadData()
            }
            }
        else{
            self.tbleView.setEmptyMessage("No Record Found!")
            }
        })
    }
    
    func acceptRejectApi(status:Int?,index:Int?){
        self.viewModel?.acceptRejectRequestApi(status: status, bookingId: self.notificationList[index ?? 0].bookId ?? "", teacherId: self.notificationList[index ?? 0].teacherId ?? "", channelName: "", accessToken: "", completion: { (data) in
            self.showToastSwift(alrtType: .success, msg: data.message ?? "", title: "")
            self.getNotificationList()
                       })
    }
    //MARK:- actions
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
}

//MARK:- TableView Delegate
extension NotificationRequestVC : UITableViewDataSource,UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return notificationList.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationRequestCell", for: indexPath)as! NotificationRequestCell
        
        cell.viewDelegate = self
        cell.btnAccept.tag = indexPath.row
        cell.btnReject.tag = indexPath.row
        cell.lblName.text = "Student Name : " +  (notificationList[indexPath.row].bookingDetail?.userDetail?.fName ?? "") + (notificationList[indexPath.row].bookingDetail?.userDetail?.lName ?? "")
        cell.lblDate.text = "Date : " + (notificationList[indexPath.row].bookingDetail?.bookingDate ?? "")
        cell.lblTime.text = "Time : " +  (notificationList[indexPath.row].bookingDetail?.timeSlot ?? "")
        if (notificationList[indexPath.row].status ?? 0 == 0){
            cell.btnAccept.isHidden = false
            cell.btnReject.isHidden = false
            cell.kStackHeight.constant = 48
            cell.kTop.constant = 18
        }
        else{
            cell.btnAccept.isHidden = true
            cell.btnReject.isHidden = true
            cell.kStackHeight.constant = 0
            cell.kTop.constant = 0
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: false)
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 93
    }
}

//MARK:- ViewDElegate
extension NotificationRequestVC : NotificationRequestDelegate{
    func accept(index:Int){
        AlertMessageWithOkCancelAction(titleStr: kAppName, messageStr: "Are you sure,you want to accept request", Target: self) { (alert) in
            if alert == KYes{
                self.acceptRejectApi(status: 1, index: index )
            }
        }
    }
    
    func reject(index:Int?){
        AlertMessageWithOkCancelAction(titleStr: kAppName, messageStr: "Are you sure,you want to reject request", Target: self) { (alert) in
                   if alert == KYes{
                    self.acceptRejectApi(status: 2, index: index ?? 0)
                       }
                   }
               }
    }


