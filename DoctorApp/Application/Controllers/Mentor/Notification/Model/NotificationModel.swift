//
//  NotificationModel.swift
//  DoctorApp
//
//  Created by Navaldeep Kaur on 26/08/20.
//  Copyright © 2020 Cerebrum Infotech. All rights reserved.
//


import Foundation


// MARK: - NotificationRequestModel
struct NotificationRequestModel: Codable {
    let code: Int?
    let success: Bool?
    let message: String?
    let data: [NotificationDatum]?
}

// MARK: - Datum
struct NotificationDatum: Codable {
    let bookId, teacherId, datumDescription: String?
    let status: Int?
    let bookingDetail: BookingDetail?

   
}

// MARK: - BookingDetail
struct BookingDetail: Codable {
    let id, studentId, bookingDate, timeSlot: String?
    let userDetail: UserDetail?

}

// MARK: - UserDetail
struct UserDetail: Codable {
    let fName, lName: String?
}


// MARK: - AcceptReject
struct AcceptRejectModel : Codable {
    let code: Int?
    let success: Bool?
    let message: String?
}
