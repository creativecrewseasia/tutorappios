//
//  FeedBackToStudent.swift
//  DoctorApp
//
//  Created by Poonam  on 01/09/20.
//  Copyright © 2020 Cerebrum Infotech. All rights reserved.
//

import UIKit
import DropDown

class FeedBackToStudent: CustomController {

    @IBOutlet weak var txtFldDate: UITextField!
    @IBOutlet weak var txtFldTips: UITextField!
    @IBOutlet weak var txtFldExercise: UITextField!
    @IBOutlet weak var txtFldProgress: UITextField!
    @IBOutlet weak var txtFldLearned: UITextField!
    @IBOutlet weak var txtFldThingNeed: UITextField!
    @IBOutlet weak var txtFldStep1: UITextField!
    @IBOutlet weak var txtfldTeacherName: UITextField!
    @IBOutlet weak var txtfldDateTime: UITextField!
    @IBOutlet weak var txtFldSubject: UITextField!
    
    @IBOutlet weak var slider: UISlider!
    
    @IBOutlet weak var lblSlider: UILabel!
    let dropDown = DropDown()
    var Prograss = ""
    var viewModel:FeedbackViewModel?
    override func viewDidLoad() {
        super.viewDidLoad()
        setView()
        setDatePickerView(self.view, type: .date)
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            self.txtFldProgress.text = item
            self.Prograss = "\(index+1)"
        }
        DropDown.startListeningToKeyboard()
    
    }
    func setView(){
        self.title = "Feedback"
    self.viewModel = FeedbackViewModel.init(view: self, delegate: self)
    }
    
    @IBAction func actnSlider(_ sender: Any) {
        lblSlider.text = "\(slider.value)"
    }
    @IBAction func actnPrograss(_ sender: Any) {
        dropDown.dataSource = ["Good","Average","Very good","Excellent"]
              dropDown.anchorView = self.txtFldProgress
              dropDown.show()
    }
    
    @IBAction func actnDate(_ sender: Any) {
        showDatePicker(datePickerDelegate: self)
    }
    
    @IBAction func actnSubmit(_ sender: Any) {
        self.viewModel?.FeedbackStudent()
        
    }
    //MARK:-For Convert Date Into String dd/mm/yyyy
       func convertDateIntoStringWithDDMMYYYY(date: Date) -> String{
           let dateFormatter = DateFormatter()
           // initially set the format based on your datepicker date / server String
           dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
           //Date which you need to convert in string
           let dateString = dateFormatter.string(from: date)
           let date = dateFormatter.date(from: dateString)
           //then again set the date format whhich type of output you need
           dateFormatter.dateFormat = "yyyy-MM-dd"
           dateFormatter.locale = Locale.current
           // again convert your date to string
           if date != nil{
               let strDate = dateFormatter.string(from: date!)
               return strDate
           }
           return ""
       }

}
extension FeedBackToStudent:SharedUIDatePickerDelegate{
    
    func doneButtonClicked(datePicker: UIDatePicker) {
        let strDate = convertDateIntoStringWithDDMMYYYY(date: datePicker.date)
            txtFldDate.text = strDate
        
    }
}
extension FeedBackToStudent:FeedbackDelegate{
    
   
}
