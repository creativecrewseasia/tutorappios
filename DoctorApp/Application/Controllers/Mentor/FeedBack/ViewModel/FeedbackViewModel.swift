//
//  FeedbackViewModel.swift
//  DoctorApp
//
//  Created by Poonam  on 02/09/20.
//  Copyright © 2020 Cerebrum Infotech. All rights reserved.
//

import Foundation
protocol FeedbackDelegate: class {
   
}
class FeedbackViewModel
{
    var view : FeedBackToStudent
    var  viewDelegate :FeedbackDelegate?
    
    init(view : FeedBackToStudent,delegate:FeedbackDelegate)
    {
        self.view = view
        self.viewDelegate = delegate
    }
    
    func FeedbackStudent()
    {
        var parm = [String:Any]()
        parm["amount"] = self.view.txtFldDate.text
        parm["datetime"] = self.view.txtfldDateTime.text
        parm["excercises"] = self.view.txtFldExercise.text
        parm["learned"] = self.view.txtFldLearned.text
        parm["parentEmail"] = "ps@gmail.com"
        parm["parentName"] = "parent"
        parm["progress"] = self.view.Prograss
        parm["rating"] = "10"
        parm["step1"] =  self.view.txtFldStep1.text
        parm["step2"] = "qqqq"
        parm["step3"] = "hjufg"
        parm["step4"] = "hjufg"
        parm["studentEmail"] = "ps@gmail.com"
        parm["studentName"] = "ps"
        parm["subject"] = "Math"
        parm["tips"] = self.view.txtFldTips.text
        parm["tutorName"] = self.view.txtfldTeacherName.text
        parm["userId"] =  AppDefaults.shared.userID
        Feedback(Params: parm)
    }
    func Feedback(Params : [String:Any])
    {
        var apiURL = APIAddress.feedbackToStudent
       
        WebService.Shared.PostApi(url: apiURL, parameter: Params , Target: self.view, showLoader: true, completionResponse: { response in
            
            Commands.println(object: response)
        }, completionnilResponse: {(error) in
            self.view.showToastSwift(alrtType: .error, msg: error, title: kOops)
        })
        
    }
}
