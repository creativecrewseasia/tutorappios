//
//  ScheduleDetail.swift
//  DoctorApp
//
//  Created by Atinder Kaur on 9/2/20.
//  Copyright © 2020 Cerebrum Infotech. All rights reserved.
//

import Foundation

// MARK: - Welcome
struct Schedule_List: Codable {
    let code: Int?
    let success: Bool?
    let message: String?
    let data: [SlotList]?
   
}

// MARK: - Datum
struct SlotList: Codable {
    let name: String?
    let slots: [Slot]?
    var collapsed: Bool?
}

// MARK: - Slot
struct Slot: Codable {
    let slot, bookings: String?
}

