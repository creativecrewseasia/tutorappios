//
//  AddScheduleViewModel.swift
//  DoctorApp
//
//  Created by Poonam  on 26/08/20.
//  Copyright © 2020 Cerebrum Infotech. All rights reserved.
//

import Foundation
protocol AddScheduleDelegate: class {
   
    func AddScheduleSuccess(data: [ScheduleResult])
    func ScheduleList(data: Schedule_List)
}


class AddScheduleViewModel
{
    var view : AddScheduleVC
    
    var delegate :  AddScheduleDelegate?
    
    init(view : AddScheduleVC,Delegate: AddScheduleDelegate)
    {
        self.view = view
        self.delegate = Delegate
    }
    
    func AddScheduleData(fromDate: String, toDate: String,slotsday: [String], slotsTime: [String]){
        let url = APIAddress.AddSchedulel
        var postData = ["fromDate": fromDate,"toDate": toDate,"slotsday": slotsday,"slotsTime": slotsTime,"teacherId": AppDefaults.shared.userID] as [String : Any]
        var urlData: URL?
        
        WebService.Shared.uploadDataMultiPart(mediaType: .none, url: url, postdatadictionary: postData, Target: self.view, showLoader: true, completionResponse: { (response) in
            
        }, completionnilResponse: { (error) in
             self.view.showToastSwift(alrtType: .error, msg: error, title: kOops)
        }) { (error) in
            self.view.showToastSwift(alrtType: .error, msg: error as! String, title: kOops)
        }
    }
    
    func getDetails(fromDate: String, toDate: String, teacherId: String) {
        let url = APIAddress.SchedulelDetail
        let postData = ["fromDate": fromDate,"toDate": toDate,"teacherId": teacherId] as [String : Any]
      
             
        WebService.Shared.PostApi(url: url, parameter: postData, Target: self.view, showLoader: true, completionResponse: { (response) in
            do
                                   {
                                       let jsonData = try JSONSerialization.data(withJSONObject: response, options: .prettyPrinted)
                                       let model = try JSONDecoder().decode(Schedule_List.self, from: jsonData)
                                       
                                    self.delegate?.ScheduleList(data: model)
                                       
                       
                                   }
                                   catch
                                   {
                                       self.view.showToastSwift(alrtType: .error, msg: kResponseNotCorrect, title: kOops)
                                   }
            
        }) { (error) in
            self.view.showToastSwift(alrtType: .error, msg: error as! String, title: kOops)
        }
    }
    
}
