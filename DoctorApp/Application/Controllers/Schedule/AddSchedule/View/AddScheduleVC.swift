//
//  AddScheduleVC.swift
//  DoctorApp
//
//  Created by Poonam  on 26/08/20.
//  Copyright © 2020 Cerebrum Infotech. All rights reserved.
//

import UIKit
import DropDown

class AddScheduleVC: CustomController {

    @IBOutlet weak var btnSchedule: CustomButton!
    @IBOutlet weak var txtFiledStartDate: UITextField!
    @IBOutlet weak var txtFieldTime: UITextField!
    @IBOutlet weak var txtFiledSelectDay: UITextField!
    @IBOutlet weak var txtFildEndDate: UITextField!
    var scheduleDetail = ""
    var fromDate = ""
    var toDate = ""
    var isSelectStartDate : Bool?
     let dropDown = DropDown()
     var str = ""
     var viewModel:AddScheduleViewModel?
    var timeArray = [String]()
    var dayArray = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        setView()
        setDatePickerView(self.view, type: .date)
        dropDown.multiSelectionAction = { [weak self] (indices, items) in
                   print("Muti selection action called with: \(items)")
                if self!.str == "day" {
                       //self?.exerciseNameTextField.setTitle("", for: .normal)
                       self!.txtFiledSelectDay.text = "\(items)"
                    self!.timeArray = items
                   }
                   else{
                  self!.txtFieldTime.text = "\(items)"
                    self!.dayArray = items
            }
        }
        // Do any additional setup after loading the view.
    }
    func setView(){ //
        
        viewModel = AddScheduleViewModel.init(view: self, Delegate: self)

        if scheduleDetail == "show" {
                self.title = "Schedule Details"
             txtFiledStartDate.text = self.fromDate
            txtFildEndDate.text = self.toDate
            btnSchedule.isHidden = true
            self.viewModel?.getDetails(fromDate: self.fromDate, toDate: self.toDate, teacherId: AppDefaults.shared.userID)
        }
        else{
                self.title = "Add Schedule"
        }
    
    }
    
    @IBAction func actionTime(_ sender: Any) {
        str = "time"
        dropDown.dataSource = ["07:00 AM","08:00 AM","09:00 AM","10:00 AM","11:00 AM","12:00 PM","1:00 PM","2:00 PM","3:00 PM","4:00 PM","5:00 PM","6:00 PM","7:00 PM","8:00 PM"]
        dropDown.anchorView = self.txtFieldTime
        dropDown.show()
    }
    
    @IBAction func actionSelectDay(_ sender: Any) {
        str = "day"
        dropDown.dataSource = ["sun","mon","tue","wed","thu","fri","sat"]
        dropDown.anchorView = self.txtFiledSelectDay
        dropDown.show()
    }
    
    @IBAction func actionStartDate(_ sender: Any) {
        isSelectStartDate = true
         showDatePicker(datePickerDelegate: self)
    }
    
    @IBAction func actionEndDate(_ sender: Any) {
        isSelectStartDate = false
         showDatePicker(datePickerDelegate: self)
    }
    
    @IBAction func actionAddSchedule(_ sender: Any) {
        self.viewModel?.AddScheduleData(fromDate: txtFiledStartDate.text ?? "", toDate: txtFildEndDate.text ?? "",slotsday: self.dayArray, slotsTime: self.timeArray)
    }
    
    
    //MARK:-For Convert Date Into String dd/mm/yyyy
    func convertDateIntoStringWithDDMMYYYY(date: Date) -> String{
        let dateFormatter = DateFormatter()
        // initially set the format based on your datepicker date / server String
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        //Date which you need to convert in string
        let dateString = dateFormatter.string(from: date)
        let date = dateFormatter.date(from: dateString)
        //then again set the date format whhich type of output you need
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.locale = Locale.current
        // again convert your date to string
        if date != nil{
            let strDate = dateFormatter.string(from: date!)
            return strDate
        }
        return ""
    }
}
extension AddScheduleVC:SharedUIDatePickerDelegate{
    
    func doneButtonClicked(datePicker: UIDatePicker) {
        let strDate = convertDateIntoStringWithDDMMYYYY(date: datePicker.date)
        if isSelectStartDate == true{
            txtFiledStartDate.text = strDate
        }else  if isSelectStartDate == false{
            txtFildEndDate.text = strDate
        }
    }
}
extension AddScheduleVC : AddScheduleDelegate{
    func ScheduleList(data: Schedule_List) {
        
        
        
    }
    
    func AddScheduleSuccess(data: [ScheduleResult]) {
        
    }
}
