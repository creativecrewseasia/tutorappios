//
//  ScheduleDetailViewModel.swift
//  DoctorApp
//
//  Created by Poonam  on 08/09/20.
//  Copyright © 2020 Cerebrum Infotech. All rights reserved.
//

import Foundation
import Foundation
protocol ScheduleDetailDelegate: class {
   
//    func AddScheduleSuccess(data: [ScheduleResult])
    func ScheduleList(data: Schedule_List)
}


class ScheduleDetailViewModel
{
    var view : ScheduleDetailVC
    
    var delegate :  ScheduleDetailDelegate?
    
    init(view : ScheduleDetailVC,Delegate: ScheduleDetailDelegate)
    {
        self.view = view
        self.delegate = Delegate
    }
    func getDetails(fromDate: String, toDate: String, teacherId: String) {
        let url = APIAddress.SchedulelDetail
        let postData = ["fromDate": fromDate,"toDate": toDate,"teacherId": teacherId] as [String : Any]
        
        
        WebService.Shared.PostApi(url: url, parameter: postData, Target: self.view, showLoader: true, completionResponse: { (response) in
            do
            {
                let jsonData = try JSONSerialization.data(withJSONObject: response, options: .prettyPrinted)
                let model = try JSONDecoder().decode(Schedule_List.self, from: jsonData)
                
                self.delegate?.ScheduleList(data: model)
            }
            catch
            {
                self.view.showToastSwift(alrtType: .error, msg: kResponseNotCorrect, title: kOops)
            }
            
        }) { (error) in
            self.view.showToastSwift(alrtType: .error, msg: error as! String, title: kOops)
        }
    }
}
