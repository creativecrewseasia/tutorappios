//
//  ScheduleDetailVC.swift
//  DoctorApp
//
//  Created by Poonam  on 03/09/20.
//  Copyright © 2020 Cerebrum Infotech. All rights reserved.
//

import UIKit

class ScheduleDetailVC: UITableViewController {
    
//    var sections = sectionsData
    var fromDate = ""
    var toDate = ""
    var slotData = [SlotList]()
     var viewModel:ScheduleDetailViewModel?
    override func viewDidLoad() {
        super.viewDidLoad()
         setView()
        // Auto resizing the height of the cell
        tableView.estimatedRowHeight = 44.0
        tableView.rowHeight = UITableView.automaticDimension
        
        self.title = "Schedule Detail"
    }
    
    func setView(){ //
           
        viewModel = ScheduleDetailViewModel.init(view: self, Delegate: self)
            self.viewModel?.getDetails(fromDate: self.fromDate, toDate: self.toDate, teacherId: AppDefaults.shared.userID)
       }
    
}

//
// MARK: - View Controller DataSource and Delegate
//
extension ScheduleDetailVC {

    override func numberOfSections(in tableView: UITableView) -> Int {
        return slotData.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return slotData[section].collapsed ?? false ? 0 : slotData[section].slots?.count ?? 0
    }
    
    // Cell
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CollapsibleTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell") as? CollapsibleTableViewCell ??
            CollapsibleTableViewCell(style: .default, reuseIdentifier: "cell")
        
        let item : Slot = (slotData[indexPath.section].slots?[indexPath.row])!
        
        cell.nameLabel.text = item.slot
//        cell.detailLabel.text = item.detail
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    // Header
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "header") as? CollapsibleTableViewHeader ?? CollapsibleTableViewHeader(reuseIdentifier: "header")
        
        header.titleLabel.text = slotData[section].name
        header.arrowLabel.text = ">"
        header.setCollapsed(slotData[section].collapsed ?? false)
        
        header.section = section
        header.delegate = self
        
        return header
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44.0
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1.0
    }

}

//
// MARK: - Section Header Delegate
//
extension ScheduleDetailVC: CollapsibleTableViewHeaderDelegate {
    
    func toggleSection(_ header: CollapsibleTableViewHeader, section: Int) {
        var collapsed = !(slotData[section].collapsed ?? false)
        
        // Toggle collapse
        slotData[section].collapsed = collapsed
        header.setCollapsed(collapsed)
        
        tableView.reloadSections(NSIndexSet(index: section) as IndexSet, with: .automatic)
    }
    
}
extension ScheduleDetailVC: ScheduleDetailDelegate {
    
  func ScheduleList(data: Schedule_List) {
    slotData = data.data!
         print("schedule data: ",slotData)
    tableView.reloadData()
     }
    
}
