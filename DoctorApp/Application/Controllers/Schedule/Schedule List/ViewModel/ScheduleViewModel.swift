//
//  ScheduleViewModel.swift
//  DoctorApp
//
//  Created by Poonam  on 26/08/20.
//  Copyright © 2020 Cerebrum Infotech. All rights reserved.
//

import Foundation
protocol ScheduleListDelegate: class {
   
    func ScheduleListSuccess(data: [ScheduleResult])
}


class ScheduleListViewModel
{
    var view : ScheduleListVC
    
    var delegate :  ScheduleListDelegate?
    
    init(view : ScheduleListVC,Delegate: ScheduleListDelegate)
    {
        self.view = view
        self.delegate = Delegate
    }
    
     func GetScheduleList()
      {
          
          let url = APIAddress.ScheduleList + AppDefaults.shared.userID
          WebService.Shared.GetApi(url: url, Target: self.view, showLoader: true, completionResponse: { (response) in
              Commands.println(object: response)
              
              do
              {
                  let jsonData = try JSONSerialization.data(withJSONObject: response, options: .prettyPrinted)
                  let model = try JSONDecoder().decode(ScheduleListModel.self, from: jsonData)
                  self.delegate?.ScheduleListSuccess(data: model.data!)
              }
              catch
              {
                  self.view.showToastSwift(alrtType: .error, msg: kResponseNotCorrect, title: kOops)
              }
          }) { (error) in
               self.view.showToastSwift(alrtType: .error, msg: error, title: kOops)
          }
       
          
      }
    
}
