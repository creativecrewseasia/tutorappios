//
//  ScheduleListModel.swift
//  DoctorApp
//
//  Created by Poonam  on 26/08/20.
//  Copyright © 2020 Cerebrum Infotech. All rights reserved.
//

import Foundation
import ObjectMapper


struct ScheduleListModel: Decodable
{
    let code: Int?
    let success: Bool?
    let message: String?
    let data: [ScheduleResult]?
}

struct ScheduleResult: Decodable
{
    let fromDate, toDate: String?

    enum CodingKeys: String, CodingKey {
        case fromDate
        case toDate
    }
    
}
