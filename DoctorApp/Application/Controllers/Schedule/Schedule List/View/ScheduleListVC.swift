//
//  ScheduleListVC.swift
//  DoctorApp
//
//  Created by Poonam  on 26/08/20.
//  Copyright © 2020 Cerebrum Infotech. All rights reserved.
//

import UIKit

class ScheduleListVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
      var viewModel:ScheduleListViewModel?
    var scheduleListArr = [ScheduleResult]()
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.separatorStyle = .none
        setView()
        self.viewModel?.GetScheduleList()
    }

    func setView(){
           viewModel = ScheduleListViewModel.init(view: self, Delegate: self)
       }
    @IBAction func actionAddSchedule(_ sender: Any) {
         let storyBoard: UIStoryboard = UIStoryboard(name: "Student", bundle: nil)
         let newViewController = storyBoard.instantiateViewController(withIdentifier: "AddScheduleVC") as! AddScheduleVC
         newViewController.modalPresentationStyle = .fullScreen
         self.navigationController?.pushViewController(newViewController, animated: true)
     }
}
///MARK:- Table view delagate

//MARK:- Table view data source
extension ScheduleListVC : UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return scheduleListArr.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellSchedule", for: indexPath) as! ScheduleCelll
       
        cell.lblFromDate.text = scheduleListArr[indexPath.row].fromDate
        cell.lblToDate.text = scheduleListArr[indexPath.row].toDate
      
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
          let storyBoard: UIStoryboard = UIStoryboard(name: "Student", bundle: nil)
               let newViewController = storyBoard.instantiateViewController(withIdentifier: "ScheduleDetailVC") as! ScheduleDetailVC
//             newViewController.scheduleDetail = "show"
        newViewController.fromDate = scheduleListArr[indexPath.row].fromDate ?? ""
        newViewController.toDate = scheduleListArr[indexPath.row].toDate ?? ""
               newViewController.modalPresentationStyle = .fullScreen
               self.navigationController?.pushViewController(newViewController, animated: true)
    }
    
}
extension ScheduleListVC : ScheduleListDelegate{
    func ScheduleListSuccess(data: [ScheduleResult]) {
        if data.count > 0 {
            scheduleListArr = data
        }
        tableView.reloadData()
    }
}
