//
//  MenuVC.swift
//  DoctorApp
//
//  Created by Poonam  on 25/08/20.
//  Copyright © 2020 Cerebrum Infotech. All rights reserved.
//

import UIKit

class MenuVC: CustomController {
    
    @IBOutlet weak var tableView: UITableView!
    
var menuArr = ["HOME","TOKEN HISTORY","ADD TOKEN","LOGOUT"]
    override func viewDidLoad() {
        super.viewDidLoad()
        if AppDefaults.shared.userTYPE == 2{
            self.menuArr = ["HOME","TOKEN HISTORY","SCHEDULE LIST","LOGOUT"]
        }
        // Do any additional setup after loading the view.
    }
    
    @IBAction func actionProfileView(_ sender: Any) {
        
      if AppDefaults.shared.userTYPE == 1{
               let storyboard = UIStoryboard.init(name: "Student", bundle: nil)
               let vc = storyboard.instantiateViewController(withIdentifier: "ProfileVC") as? ProfileVC
               let frontVC = revealViewController().frontViewController as? UINavigationController
               frontVC?.pushViewController(vc!, animated: false)
               revealViewController().pushFrontViewController(frontVC, animated: true)
            }else  if AppDefaults.shared.userTYPE == 2{
               let storyboard = UIStoryboard.init(name: "Student", bundle: nil)
               let vc = storyboard.instantiateViewController(withIdentifier: "TeacherProfileVC") as? TeacherProfileVC
               let frontVC = revealViewController().frontViewController as? UINavigationController
               frontVC?.pushViewController(vc!, animated: false)
               revealViewController().pushFrontViewController(frontVC, animated: true)
           }
    }
    
}
///MARK:- Table view delagate
extension MenuVC : UITableViewDelegate{
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    }
    
      //Did Select Row
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        switch menuArr[indexPath.row]
        {
           case "HOME":
             if AppDefaults.shared.userTYPE == 1{
                let storyboard = UIStoryboard.init(name: "Student2", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "DashBoardVC") as? DashBoardVC
                let frontVC = revealViewController().frontViewController as? UINavigationController
                frontVC?.pushViewController(vc!, animated: false)
                revealViewController().pushFrontViewController(frontVC, animated: true)
                break
             }else if AppDefaults.shared.userTYPE == 2{
                let storyboard = UIStoryboard.init(name: "Mentor", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "DashBoardVC") as? MentorDashboardVC
                let frontVC = revealViewController().frontViewController as? UINavigationController
                frontVC?.pushViewController(vc!, animated: false)
                revealViewController().pushFrontViewController(frontVC, animated: true)
                break
             }
            
        case "TOKEN HISTORY":
            let storyboard = UIStoryboard.init(name: "Student", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "TokenHistoryVC") as? TokenHistoryVC
            let frontVC = revealViewController().frontViewController as? UINavigationController
            frontVC?.pushViewController(vc!, animated: false)
            revealViewController().pushFrontViewController(frontVC, animated: true)
            break
            
        case "ADD TOKEN":
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "AddTokenListVC") as? AddTokenListVC
            strFromSideMenu = "sideMenu"
            let frontVC = revealViewController().frontViewController as? UINavigationController
            frontVC?.pushViewController(vc!, animated: false)
            revealViewController().pushFrontViewController(frontVC, animated: true)
            break
            
        case "SCHEDULE LIST":
            let storyboard = UIStoryboard.init(name: "Student", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ScheduleListVC") as? ScheduleListVC
            let frontVC = revealViewController().frontViewController as? UINavigationController
            frontVC?.pushViewController(vc!, animated: false)
            revealViewController().pushFrontViewController(frontVC, animated: true)
            break
            
        case "LOGOUT":
            // create an actionSheet
            let actionSheetController: UIAlertController = UIAlertController(title: kAppName, message: "Do you want to logout?", preferredStyle: .actionSheet)
            // create an action
            let firstAction: UIAlertAction = UIAlertAction(title: "Yes", style: .default) { action -> Void in
                self.emptyData()
                configs.kAppdelegate.setRootViewController()
            }
            
            let cancelAction: UIAlertAction = UIAlertAction(title: "No", style: .cancel) { action -> Void in }
//            / add actions
            actionSheetController.addAction(firstAction)
            actionSheetController.addAction(cancelAction)
            actionSheetController.popoverPresentationController?.sourceView = self.view // works for both iPhone & iPad
            present(actionSheetController, animated: true)
            {
                print("option menu presented")
            }
          
            break
        default:
            print("NORecord")
            break
            
        }
    }
    
    func emptyData()
       {
           AppDefaults.shared.userID = "0"
           AppDefaults.shared.userTYPE = 0
           AppDefaults.shared.userName = ""
           AppDefaults.shared.userFirstName = ""
           AppDefaults.shared.userLastName = ""
           AppDefaults.shared.userImage = ""
           AppDefaults.shared.userEmail = ""
           AppDefaults.shared.userJWT_Token = ""
           AppDefaults.shared.firebaseVID = ""
           
           AppDefaults.shared.userPhoneNumber = ""
           AppDefaults.shared.userCountryCode = ""
           AppDefaults.shared.app_LATITUDE = "0.0"
           AppDefaults.shared.app_LONGITUDE = "0.0"
           
          
           AppDefaults.shared.userDOB = ""
        
       }
}

//MARK:- Table view data source
extension MenuVC : UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuArr.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MenuCell
        cell.lblMenuTitle.text = menuArr[indexPath.row]
        return cell
    }
}
