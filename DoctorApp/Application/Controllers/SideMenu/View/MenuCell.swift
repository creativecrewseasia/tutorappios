//
//  MenuCell.swift
//  DoctorApp
//
//  Created by Poonam  on 25/08/20.
//  Copyright © 2020 Cerebrum Infotech. All rights reserved.
//

import Foundation

import UIKit

class MenuCell: UITableViewCell {
    
    @IBOutlet weak var lblMenuTitle: UILabel!
}
