//
//  TokenHistoryViewModel.swift
//  DoctorApp
//
//  Created by Poonam  on 25/08/20.
//  Copyright © 2020 Cerebrum Infotech. All rights reserved.
//

import Foundation

protocol TokenHistoryDelegate: class {
   
    func HistoryTokenListSuccess(data: [TokenDataResult])
}


class TokenHistoryViewModel
{
    var view : TokenHistoryVC
    
    var delegate :  TokenHistoryDelegate?
    
    init(view : TokenHistoryVC,Delegate: TokenHistoryDelegate)
    {
        self.view = view
        self.delegate = Delegate
    }
    
    func GetHistoryToken()
    {
        
        let url = APIAddress.TokenHistory + AppDefaults.shared.userID
        WebService.Shared.GetApi(url: url, Target: self.view, showLoader: true, completionResponse: { (response) in
            Commands.println(object: response)
            
            do
            {
                let jsonData = try JSONSerialization.data(withJSONObject: response, options: .prettyPrinted)
                let model = try JSONDecoder().decode(TokenHistoryModel.self, from: jsonData)
                self.delegate?.HistoryTokenListSuccess(data: model.data!)
            }
            catch
            {
                self.view.showToastSwift(alrtType: .error, msg: kResponseNotCorrect, title: kOops)
            }
        }) { (error) in
             self.view.showToastSwift(alrtType: .error, msg: error, title: kOops)
        }
     
        
    }
    
    //MARK:- TeacherToken
    func GetTeacherHistoryToken()
       {
           let url = APIAddress.teachetTookenHistory + AppDefaults.shared.userID
           WebService.Shared.GetApi(url: url, Target: self.view, showLoader: true, completionResponse: { (response) in
               Commands.println(object: response)
               
               do
               {
                   let jsonData = try JSONSerialization.data(withJSONObject: response, options: .prettyPrinted)
                   let model = try JSONDecoder().decode(TokenHistoryModel.self, from: jsonData)
                   self.delegate?.HistoryTokenListSuccess(data: model.data!)
               }
               catch
               {
                   self.view.showToastSwift(alrtType: .error, msg: kResponseNotCorrect, title: kOops)
               }
           }) { (error) in
                self.view.showToastSwift(alrtType: .error, msg: error, title: kOops)
                self.view.tableView.setEmptyMessage("No Record Found!")
           }
        
           
       }
    
}
