//
//  HistoryTokenCell.swift
//  DoctorApp
//
//  Created by Poonam  on 25/08/20.
//  Copyright © 2020 Cerebrum Infotech. All rights reserved.
//

import Foundation
import UIKit

class HistoryTokenCell: UITableViewCell {

    
    @IBOutlet weak var lblTokenBal: UILabel!
    @IBOutlet weak var lblToken: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblType: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setCellUI(){
        

    }
    
}
