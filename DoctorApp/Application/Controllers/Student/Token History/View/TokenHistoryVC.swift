//
//  TokenHistoryVC.swift
//  DoctorApp
//
//  Created by Poonam  on 25/08/20.
//  Copyright © 2020 Cerebrum Infotech. All rights reserved.
//

import UIKit

class TokenHistoryVC: CustomController {

    @IBOutlet weak var tableView: UITableView!
     var viewModel:TokenHistoryViewModel?
    var tokenHistoryArr = [TokenDataResult]()
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.separatorStyle = .none
        // Do any additional setup after loading the view.
        setView()
         if AppDefaults.shared.userTYPE == 2
               {
                   self.viewModel?.GetTeacherHistoryToken()
               }
               else{
                self.viewModel?.GetHistoryToken()
               }
    }

    func setView(){
        viewModel = TokenHistoryViewModel.init(view: self, Delegate: self)
    }

}
///MARK:- Table view delagate
extension TokenHistoryVC : UITableViewDelegate{
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        cell.separatorInset = UIEdgeInsets.zero
//        cell.layoutMargins = UIEdgeInsets.zero
    }
}

//MARK:- Table view data source
extension TokenHistoryVC : UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tokenHistoryArr.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TokenCell", for: indexPath) as! HistoryTokenCell
        cell.lblTokenBal.text = "\(tokenHistoryArr[indexPath.row].tokenBalance!)"
        cell.lblToken.text = tokenHistoryArr[indexPath.row].token
        cell.lblDate.text = tokenHistoryArr[indexPath.row].createdAt
        cell.lblType.text = tokenHistoryArr[indexPath.row].type
        return cell
    }
}

extension TokenHistoryVC : TokenHistoryDelegate{
    func HistoryTokenListSuccess(data: [TokenDataResult]) {
        if data.count > 0 {
            tokenHistoryArr = data
        }
        tableView.reloadData()
    }
}
