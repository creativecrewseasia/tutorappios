//
//  TokenHistoryModel.swift
//  DoctorApp
//
//  Created by Poonam  on 25/08/20.
//  Copyright © 2020 Cerebrum Infotech. All rights reserved.
//

import Foundation
import ObjectMapper


struct TokenHistoryModel: Decodable
{
    let code: Int?
    let success: Bool?
    let message: String?
    let data: [TokenDataResult]?
}

struct TokenDataResult: Decodable
{
    let token, type, createdAt: String?
    let tokenBalance: Int?

    enum CodingKeys: String, CodingKey {
        case token
        case type
        case createdAt, tokenBalance
    }
    
}
