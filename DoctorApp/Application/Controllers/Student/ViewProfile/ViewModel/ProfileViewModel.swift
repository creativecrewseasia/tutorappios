//
//  ProfileViewModel.swift
//  DoctorApp
//
//  Created by Poonam  on 25/08/20.
//  Copyright © 2020 Cerebrum Infotech. All rights reserved.
//

import Foundation
protocol ProfileDelegate: class {
   
    func GetProfileSuccess(data: DataResult)
}


class ProfileViewModel
{
    var view : ProfileVC
    
    var delegate :  ProfileDelegate?
    
    init(view : ProfileVC,Delegate: ProfileDelegate)
    {
        self.view = view
        self.delegate = Delegate
    }
    
    func UpdateProfile(){
        var parm = [String:Any]()
        parm["lName"] = self.view.txtFieldLastNAme.text
        parm["fName"] = self.view.txtFieldFirstName.text
        parm["dob"] = self.view.txtFildDOB.text
        parm["info"] = self.view.txtfildInfo.text
        parm["grade"] = self.view.txtfildGrade.text
        parm["needed"] = self.view.txtFildNeeded.text
        parm["help"] =  self.view.txtFildHelp.text
        parm["superhero"] = self.view.txtfildSuperhero.text
        parm["userId"] = AppDefaults.shared.userID
        parm["summery"] = self.view.txtViewSummery.text
        UpdateProfiles(Params: parm)
    }
    
    func UpdateProfiles(Params : [String:Any]){
        let url = APIAddress.UpdateProfile
     WebService.Shared.PostApi(url: url, parameter: Params , Target: self.view, showLoader: true, completionResponse: { response in
            
            Commands.println(object: response)
        do
        {
            let data = response as? [String : Any]
            if let code = data?["code"] as? Int  {
                if code == 200 {
                    self.view.AlertMessageWithOkAction(titleStr: kAppName, messageStr: "profile update successfully.", Target: self.view)
                    {
                        self.view.navigationController?.popViewController(animated: true)
                    }
                }
            }
        }
        catch
        {
            self.view.showToastSwift(alrtType: .error, msg: kResponseNotCorrect, title: kOops)
        }
        }, completionnilResponse: {(error) in
            self.view.showToastSwift(alrtType: .error, msg: error, title: kOops)
        })
        
    }
    
    
    func GetProfileDetail()
    {
        
        let url = APIAddress.GetProfile + AppDefaults.shared.userID
    WebService.Shared.GetApi(url: url, Target: self.view, showLoader: true, completionResponse: { (response) in
            Commands.println(object: response)
            
            do
            {
                let jsonData = try JSONSerialization.data(withJSONObject: response, options: .prettyPrinted)
                let model = try JSONDecoder().decode(GetProfileModel.self, from: jsonData)
                
                //                AppDefaults.shared.userName = model.data.userName ?? ""
                //                AppDefaults.shared.userJWT_Token = model.data.authToken ?? ""
                //                AppDefaults.shared.userEmail = self.view.tfEmail.text ?? ""
                //                AppDefaults.shared.userID = model.data.userID ?? "0"
                self.delegate?.GetProfileSuccess(data: model.data!)
            }
            catch
            {
                self.view.showToastSwift(alrtType: .error, msg: kResponseNotCorrect, title: kOops)
            }
        }) { (error) in
            self.view.showToastSwift(alrtType: .error, msg: error, title: kOops)
        }
        
        
    }
    
}
