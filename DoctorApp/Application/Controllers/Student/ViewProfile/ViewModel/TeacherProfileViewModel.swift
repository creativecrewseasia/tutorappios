//
//  TeacherProfileViewModel.swift
//  DoctorApp
//
//  Created by Poonam  on 26/08/20.
//  Copyright © 2020 Cerebrum Infotech. All rights reserved.
//

import Foundation

protocol TeacherProfileDelegate: class {
   
    func GetProfileSuccess(data: DataResult)
}


class TeacherProfileViewModel
{
    var view : TeacherProfileVC
    
    var delegate :  TeacherProfileDelegate?
    
    init(view : TeacherProfileVC,Delegate: TeacherProfileDelegate)
    {
        self.view = view
        self.delegate = Delegate
    }
    
    func GetProfileDetail()
    {
        
        let url = APIAddress.GetProfile + AppDefaults.shared.userID
    WebService.Shared.GetApi(url: url, Target: self.view, showLoader: true, completionResponse: { (response) in
            Commands.println(object: response)
            
            do
            {
                let jsonData = try JSONSerialization.data(withJSONObject: response, options: .prettyPrinted)
                let model = try JSONDecoder().decode(GetProfileModel.self, from: jsonData)
                
                //                AppDefaults.shared.userName = model.data.userName ?? ""
                //                AppDefaults.shared.userJWT_Token = model.data.authToken ?? ""
                //                AppDefaults.shared.userEmail = self.view.tfEmail.text ?? ""
                //                AppDefaults.shared.userID = model.data.userID ?? "0"
                self.delegate?.GetProfileSuccess(data: model.data!)
            }
            catch
            {
                self.view.showToastSwift(alrtType: .error, msg: kResponseNotCorrect, title: kOops)
            }
        }) { (error) in
            self.view.showToastSwift(alrtType: .error, msg: error, title: kOops)
        }
        
        
    }
    
}
