//
//  GetProfileModel.swift
//  DoctorApp
//
//  Created by Poonam  on 25/08/20.
//  Copyright © 2020 Cerebrum Infotech. All rights reserved.
//

import Foundation
import ObjectMapper


struct GetProfileModel: Decodable
{
    let code: Int?
    let success: Bool?
    let message: String?
    let data: DataResult?
}

struct DataResult: Decodable
{
    let Subject : Subject?
    let userBankdetail: userBankdetail?
     let userdetails: ProfileDataResult?
}

struct Subject: Decodable
{
   
}
struct userBankdetail: Decodable
{
   let accountNumber, bankName : String?
   let ifcCode ,userName: String?

   enum CodingKeys: String, CodingKey {
       case accountNumber
       case bankName,ifcCode
       case userName
   }
}


struct ProfileDataResult: Decodable
{
    let id, email : String?
    let userDetail: userDetail?

    enum CodingKeys: String, CodingKey {
        case id
        case email
        case userDetail
    }
    
}
struct userDetail: Decodable
{
    let fName, lName, dob, uniqueId ,rememberToken, info, grade, needed, help, superhero, summery, image : String?
    let planId: Int?

    enum CodingKeys: String, CodingKey {
        case fName, lName, dob , uniqueId, rememberToken
        case info
        case grade
        
        case needed
        case help
        
        case superhero
        case summery
        
        case image
        case planId
    }
}

