//
//  TeacherProfileVC.swift
//  DoctorApp
//
//  Created by Poonam  on 26/08/20.
//  Copyright © 2020 Cerebrum Infotech. All rights reserved.
//

import UIKit

class TeacherProfileVC: UIViewController {
    
    @IBOutlet weak var txtViewSummery: UITextView!
    @IBOutlet weak var txtFieldFirstName: UITextField!
    @IBOutlet weak var txtFieldLastNAme: UITextField!
    @IBOutlet weak var txtFildDOB: UITextField!
    @IBOutlet weak var txtfildGrade: UITextField!
    @IBOutlet weak var txtfildSuperhero: UITextField!
    @IBOutlet weak var txtFildHelp: UITextField!
    @IBOutlet weak var txtFildNeeded: UITextField!
    @IBOutlet weak var txtfildInfo: UITextField!
    @IBOutlet weak var txtFildContact2: UITextField!
    @IBOutlet weak var txtFiledContact1: UITextField!
    
    @IBOutlet weak var txtFldUserName: UITextField!
    @IBOutlet weak var txtFldIFCCode: UITextField!
    @IBOutlet weak var txtFldBankName: UITextField!
    @IBOutlet weak var accountNumber: UITextField!
    var viewModel:TeacherProfileViewModel?
    var profileData : ProfileDataResult?
     var bankData : userBankdetail?
    override func viewDidLoad() {
        super.viewDidLoad()

        setView()
       self.viewModel?.GetProfileDetail()
    }
    func setView(){
         viewModel = TeacherProfileViewModel.init(view: self, Delegate: self)
     }
    
    func setProfileData(data: ProfileDataResult){
        txtViewSummery.text  = data.userDetail?.summery
        txtFieldFirstName.text = data.userDetail?.fName
        txtFieldLastNAme.text = data.userDetail?.lName
        txtFildDOB.text = data.userDetail?.dob
        txtFiledContact1.text = data.email
        txtFildContact2.text = data.userDetail?.rememberToken
        accountNumber.text = bankData?.accountNumber
        txtFldBankName.text = bankData?.bankName
        txtFldIFCCode.text = bankData?.ifcCode
        txtFldUserName.text = bankData?.userName
        
    }
}
extension TeacherProfileVC  : TeacherProfileDelegate{
    func GetProfileSuccess(data: DataResult) {
        if data != nil {
            profileData = data.userdetails
            bankData = data.userBankdetail
            setProfileData(data: profileData!)
        }
    }
}
