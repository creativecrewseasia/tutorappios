//
//  ProfileVC.swift
//  DoctorApp
//
//  Created by Poonam  on 25/08/20.
//  Copyright © 2020 Cerebrum Infotech. All rights reserved.
//

import UIKit

class ProfileVC: UIViewController {
    
    @IBOutlet weak var txtViewSummery: UITextView!
    @IBOutlet weak var txtFieldFirstName: UITextField!
    @IBOutlet weak var txtFieldLastNAme: UITextField!
    @IBOutlet weak var txtFildDOB: UITextField!
    @IBOutlet weak var txtfildGrade: UITextField!
    @IBOutlet weak var txtfildSuperhero: UITextField!
    @IBOutlet weak var txtFildHelp: UITextField!
    @IBOutlet weak var txtFildNeeded: UITextField!
    @IBOutlet weak var txtfildInfo: UITextField!
    @IBOutlet weak var txtFildContact2: UITextField!
    @IBOutlet weak var txtFiledContact1: UITextField!
    
    var viewModel:ProfileViewModel?
    var profileData : ProfileDataResult?
    override func viewDidLoad() {
        super.viewDidLoad()

        setView()
       self.viewModel?.GetProfileDetail()
    }
    func setView(){
         viewModel = ProfileViewModel.init(view: self, Delegate: self)
     }
    
    func setProfileData(data: ProfileDataResult){
        txtViewSummery.text  = data.userDetail?.summery
        txtFieldFirstName.text = data.userDetail?.fName
        txtFieldLastNAme.text = data.userDetail?.lName
        txtFildDOB.text = data.userDetail?.dob
        txtfildInfo.text = data.userDetail?.info
        txtfildGrade.text = data.userDetail?.grade
        txtFildNeeded.text = data.userDetail?.needed
        txtFildHelp.text = data.userDetail?.help
        txtfildSuperhero.text = data.userDetail?.superhero
        txtFiledContact1.text = data.email
        txtFildContact2.text = data.userDetail?.rememberToken
        
    }
    
    @IBAction func actnSubmit(_ sender: Any) {
        self.viewModel?.UpdateProfile()
    }
    
    
}
extension ProfileVC : ProfileDelegate{
    func GetProfileSuccess(data: DataResult) {
        if data != nil {
            profileData = data.userdetails
            setProfileData(data: profileData!)
        }
    }
}
