//
//  RoleVC.swift
//  DoctorApp
//
//  Created by Poonam  on 31/08/20.
//  Copyright © 2020 Cerebrum Infotech. All rights reserved.
//

import UIKit

class RoleVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    @IBAction func actnTeacher(_ sender: Any) {
        let controller = Navigation.GetInstance(of: .TeacherSignupVC)as! TeacherSignupVC
        self.push_To_Controller(from_controller: self, to_Controller: controller)
    }
    
    @IBAction func actnStudent(_ sender: Any) {
        let controller = Navigation.GetInstance(of: .SignupVC)as! SignupVC
        self.push_To_Controller(from_controller: self, to_Controller: controller)
    }
    @IBAction func actnBack(_ sender: Any) {
          self.moveBACK(controller: self)
    }
    
}
