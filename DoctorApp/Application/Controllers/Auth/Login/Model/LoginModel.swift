//
//  LoginModel.swift
//  DoctorApp
//
//  Created by Mohit Sharma on 8/19/20.
//  Copyright © 2020 Cerebrum Infotech. All rights reserved.
//

import Foundation
import ObjectMapper


struct LoginResponseModel: Decodable
{
    let code: Int?
    let success: Bool?
    let message: String?
    let data: DataLogin
}

struct DataLogin: Decodable
{
    let userName, userID, authToken: String?
    let type: Int?

    enum CodingKeys: String, CodingKey {
        case userName
        case userID = "userId"
        case authToken, type
    }
    
}
