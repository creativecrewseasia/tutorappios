//
//  ViewController.swift
//  DoctorApp
//
//  Created by Mohit Sharma on 8/17/20.
//  Copyright © 2020 Cerebrum Infotech. All rights reserved.
//

import UIKit
import ValidationTextField
import WebKit

class LoginVC: UIViewController
{
    
    @IBOutlet var ivLogo: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet var webVIEW: UIWebView!
    @IBOutlet var tfEmail: ValidationTextField!
    @IBOutlet var tfPassword: ValidationTextField!
    @IBOutlet var btnLogin: ButtonWithShadowAndRadious!
    @IBOutlet var btnSignup: UIButton!
    
    var viewModel:LoginVC_ViewModel?
    
    var isValid: Bool?
    {
        didSet
        {
            btnLogin.isEnabled = isValid ?? false
            btnLogin.backgroundColor = isValid ?? false ? Appcolor.kYellowTheme : .lightGray
        }
    }
    
    var validDic = ["email":false,"password": false]
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.viewModel = LoginVC_ViewModel.init(view: self)
        lblTitle.textColor = Appcolor.kTheme_Color
        Appcolor.update_ThemeColor()
        //  self.tfEmail.text = AppDefaults.shared.userEmail
        validateEntries()
        if AppDefaults.shared.userID == "0"
        {
            
        }
        else
        {
            if AppDefaults.shared.userTYPE == 2
            {
                self.setRootView("SWRevealVC", storyBoard: "Mentor")
            }
            else{
                self.setRootView("SWRevealVC", storyBoard: "Student2")

            }
        }
        
        // let rqst = URLRequest(url: URL(string: "https://conference.infinitywebtechnologies.com/superhero/")!)
        // self.webVIEW.loadRequest(rqst)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
    }
    
    @IBAction func acnLogin(_ sender: UIButton)
    {
        if(self.tfEmail.text?.count ?? 0 == 0)
        {
            self.showToastSwift(alrtType: .statusOrange, msg: AlertTitles.Enter_Email, title: "")
        }
        else if(self.tfPassword.text?.count ?? 0 == 0)
        {
            self.showToastSwift(alrtType: .statusOrange, msg: AlertTitles.Enter_Password, title: "")
        }
        else
        {
            var parm = [String:Any]()
            parm["email"] = self.tfEmail.text
            parm["password"] = self.tfPassword.text
            self.view.endEditing(true)
            viewModel?.Login(Params: parm)
        }
    }
    
    @IBAction func goToSignUp(_ sender: Any)
    {
        let controller = Navigation.GetInstance(of: .RoleVC)as! RoleVC
        self.push_To_Controller(from_controller: self, to_Controller: controller)
        // self.setRootView("SignupVC", storyBoard: "Main")
        
     
    }
    
    
    func validateEntries()
    {
        tfEmail.validCondition = {$0.count > 5 && $0.contains("@") && $0.contains(".")}
        tfPassword.validCondition = {$0.count > 3}
        
        tfEmail.successImage = UIImage(named: "success")
        tfEmail.errorImage = UIImage(named: "error")
        
        tfPassword.successImage = UIImage(named: "success")
        tfPassword.errorImage = UIImage(named: "error")
        
        tfEmail.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        tfPassword.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        self.btnLogin.backgroundColor = Appcolor.kTextColorGray
        //self.ivLogo.Rounded()
        self.btnLogin.updateLayerProperties()
    }
    
    
    @objc func textFieldDidChange(_ textfield: UITextField)
    {
        let tf = textfield as! ValidationTextField
        
        switch tf
        {
        case tfEmail:
            validDic["email"] = tf.isValid
        case tfPassword:
            validDic["password"] = tf.isValid
        default:
            break
        }
        
        isValid = validDic.reduce(true){ $0 && $1.value}
    }
    
    
    
}

