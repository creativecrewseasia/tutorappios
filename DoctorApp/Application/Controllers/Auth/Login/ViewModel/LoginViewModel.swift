//
//  LoginViewModel.swift
//  DoctorApp
//
//  Created by Mohit Sharma on 8/18/20.
//  Copyright © 2020 Cerebrum Infotech. All rights reserved.
//

import Foundation


class LoginVC_ViewModel
{
    var view : LoginVC
    
    init(view : LoginVC)
    {
        self.view = view
    }
    
    func Login(Params : [String:Any])
    {
        WebService.Shared.PostApi(url: APIAddress.LOGIN, parameter: Params , Target: self.view, showLoader: true, completionResponse: { response in
            
            Commands.println(object: response)
            
            do
            {
                let jsonData = try JSONSerialization.data(withJSONObject: response, options: .prettyPrinted)
                let model = try JSONDecoder().decode(LoginResponseModel.self, from: jsonData)

                AppDefaults.shared.userName = model.data.userName ?? ""
                AppDefaults.shared.userJWT_Token = model.data.authToken ?? ""
                AppDefaults.shared.userEmail = self.view.tfEmail.text ?? ""
                AppDefaults.shared.userID = model.data.userID ?? "0"
                AppDefaults.shared.userTYPE = model.data.type ?? 0

                configs.kAppdelegate.setRootViewController()
                self.view.showToastSwift(alrtType: .success, msg: "Login successfully", title: "")
            }
            catch
            {
                self.view.showToastSwift(alrtType: .error, msg: kResponseNotCorrect, title: kOops)
            }
            
        }, completionnilResponse: {(error) in
            self.view.showToastSwift(alrtType: .error, msg: error, title: kOops)
        })
        
    }
    
    
}

