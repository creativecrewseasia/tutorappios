//
//  TeacherSignUpViewModel.swift
//  DoctorApp
//
//  Created by Poonam  on 31/08/20.
//  Copyright © 2020 Cerebrum Infotech. All rights reserved.
//

import Foundation


protocol TeacherSignupDelegate: class {
   
     func getSubjectList (result : subjectData)
}
class TeacherSignUpViewModel
{
    var view : TeacherSignupVC
    var  viewDelegate :TeacherSignupDelegate?
    
    init(view : TeacherSignupVC,delegate:TeacherSignupDelegate)
    {
        self.view = view
        self.viewDelegate = delegate
    }
    
    func SignupTeacher()
      {
          var parm = [String:Any]()
        parm["fName"] = self.view.txtFildFirstName.text
        parm["password"] = self.view.txtFildPasswrd.text
        parm["lName"] = self.view.txtFildLastName.text
        parm["teacherId"] = "123456"
        parm["email"] = self.view.txtFildEmail.text
        parm["education"] = self.view.txtFildEducation.text
        parm["address"] = self.view.txtFildAddress.text
        parm["grade"] = self.view.gradeStr
        parm["hours"] =  self.view.txtFildHours.text
        parm["arrested"] = "0"
        parm["arrestedDetails"] = ""
        parm["charged"] = "0"
        parm["chargedDetails"] = ""
        parm["bankName"] = self.view.txtFildBankName.text
        parm["userName"] = self.view.txtFildUserName.text
        parm["ifcCode"] = self.view.txtFildIFC.text
        parm["accountNumber"] = self.view.txtFildAccountNumber.text
        parm["license"] =  self.view.imgUrl
        parm["documents"] = self.view.certiBase64
        parm["report"] = self.view.reportBase64
        parm["cv"] =  self.view.cvBase64
        parm["phoneNo"] =  self.view.textFldPhnNum.text
        parm["subjects"] =  ["11bf5b37-e0b8-42e0-8dcf-dc8c4aefc000"]
        parm["over18"] =  "Yes"
          Signup(Params: parm)
      }
    
    //MARK:- GetSubjectList
    func getSubjectList() {
        
        WebService.Shared.GetApi(url: APIAddress.SubjecList , Target: self.view, showLoader: false, completionResponse: { (response) in
            
            Commands.println(object: response)
            do
            {
                let jsonData = try JSONSerialization.data(withJSONObject: response, options: .prettyPrinted)
                let model = try JSONDecoder().decode(subjectData.self, from: jsonData)
                
                self.viewDelegate?.getSubjectList(result: model)
                
            }
            catch
            {
                //  self.view.showToastSwift(alrtType: .error, msg: kResponseNotCorrect, title: kOops)
            }
            
        }) { (error) in
            //  self.view.showToastSwift(alrtType: .error, msg: error, title: kOops)
        }
    }
    
    func Signup(Params : [String:Any])
      {
              var apiURL  = APIAddress.teacherSignup
        
        WebService.Shared.uploadDataMultiPartTeacherSignup(mediaType: .Image, url: apiURL, postdatadictionary: Params, Target: self.view, showLoader: false, completionResponse: { (response) in
             Commands.println(object: response)
            do
            {
                let jsonData = try JSONSerialization.data(withJSONObject: response, options: .prettyPrinted)
                let model = try JSONDecoder().decode(SignupResponseModel.self, from: jsonData)
                AppDefaults.shared.userName = model.data.userName ?? ""
                AppDefaults.shared.userJWT_Token = model.data.authToken ?? ""
                AppDefaults.shared.userEmail = self.view.txtFildEmail.text ?? ""
                AppDefaults.shared.userID = model.data.userID ?? "0"
                configs.kAppdelegate.setRootViewController()
                self.view.showToastSwift(alrtType: .success, msg: "Login successfully", title: "")
            }
            catch
            {
                self.view.showToastSwift(alrtType: .error, msg: kResponseNotCorrect, title: kOops)
            }
        }, completionnilResponse: { (error) in
             self.view.showToastSwift(alrtType: .error, msg: error, title: kOops)
        }) { (error) in
            self.view.showToastSwift(alrtType: .error, msg: error as! String, title: kOops)
        }
          
      }
}
