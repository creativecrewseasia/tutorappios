//
//  SignupViewModel.swift
//  DoctorApp
//
//  Created by Mohit Sharma on 8/19/20.
//  Copyright © 2020 Cerebrum Infotech. All rights reserved.
//

import Foundation


protocol SignupDelegate: class {
   
     func getSubjectList (result : subjectData)
    func getFacultyDesigList(result: TeacherFacultyModel)
}
class SignupVC_ViewModel
{
    var view : SignupVC
    var  viewDelegate :SignupDelegate?
    
    init(view : SignupVC,delegate:SignupDelegate)
    {
        self.view = view
        self.viewDelegate = delegate
    }
    
    
    func SignupMentor()
    {
        var parm = [String:Any]()
        parm["email"] = self.view.tfEmail.text
        parm["fName"] = self.view.tfUserName.text
        parm["password"] = self.view.tfPassword.text
        parm["lName"] = self.view.textLastName.text
        parm["teacherId"] = "12345"
        parm["dob"] = "2020-04-25"
        parm["faculty"] = self.view.facultyData[self.view.facultyIndex].id
        parm["designation"] = self.view.designationData[self.view.designationIndex].id
        parm["subjects"] =  self.view.selectedSubIds
        Signup(Params: parm)
    }
    
    func SignupStudent()
    {
        var parm = [String:Any]()
        parm["email"] = self.view.tfEmail.text
        parm["fName"] = self.view.tfUserName.text
        parm["password"] = self.view.tfPassword.text
        parm["lName"] = self.view.textLastName.text
        parm["rollNo"] = "12"
        parm["dob"] = "2020-04-25"
        parm["class"] = "11bf5b37-e0b8-42e0-8dcf-dc8c4aefc000"
        parm["house"] = "11bf5b37-e0b8-42e0-8dcf-dc8c4aefc000"
        parm["section"] = "11bf5b37-e0b8-42e0-8dcf-dc8c4aefc000"
        
        parm["planId"] = SignupVC.planId
        parm["amount"] = SignupVC.ammount
        parm["info"] = self.view.textInfo.text
        parm["grade"] = self.view.textGrade.text
        
        parm["needed"] = self.view.textNeeded.text
        parm["help"] = self.view.textHelp.text
        parm["superhero"] = self.view.textFavSuperHero.text
        
        Signup(Params: parm)
    }
    
    func Signup(Params : [String:Any])
    {
        var apiURL = APIAddress.STUDENT_REGISTRATION
        if(self.view.type == 2)
        {
           apiURL = APIAddress.MENTOR_REGISTRATION
        }
        WebService.Shared.PostApi(url: apiURL, parameter: Params , Target: self.view, showLoader: true, completionResponse: { response in
            
            Commands.println(object: response)
            
            do
            {
                let jsonData = try JSONSerialization.data(withJSONObject: response, options: .prettyPrinted)
                let model = try JSONDecoder().decode(SignupResponseModel.self, from: jsonData)

                AppDefaults.shared.userName = model.data.userName ?? ""
                AppDefaults.shared.userJWT_Token = model.data.authToken ?? ""
                AppDefaults.shared.userEmail = self.view.tfEmail.text ?? ""
                AppDefaults.shared.userID = model.data.userID ?? "0"

                configs.kAppdelegate.setRootViewController()
                self.view.showToastSwift(alrtType: .success, msg: "Login successfully", title: "")
            }
            catch
            {
                self.view.showToastSwift(alrtType: .error, msg: kResponseNotCorrect, title: kOops)
            }
            
        }, completionnilResponse: {(error) in
            self.view.showToastSwift(alrtType: .error, msg: error, title: kOops)
        })
        
    }
    
    //MARK:- GetSubjectList
    func getSubjectList() {
        WebService.Shared.GetApi(url: APIAddress.SubjecList , Target: self.view, showLoader: false, completionResponse: { (response) in
                   
                    Commands.println(object: response)
                              do
                               {
                                   let jsonData = try JSONSerialization.data(withJSONObject: response, options: .prettyPrinted)
                                   let model = try JSONDecoder().decode(subjectData.self, from: jsonData)
                                   
                                   self.viewDelegate?.getSubjectList(result: model)
                   
                               }
                               catch
                               {
                                 //  self.view.showToastSwift(alrtType: .error, msg: kResponseNotCorrect, title: kOops)
                               }
                   
               }) { (error) in
                 //  self.view.showToastSwift(alrtType: .error, msg: error, title: kOops)
               }
    }
    
    //MARK:- GetFacultyAndDEsignationList
    func getFacultyDesigList() {
        WebService.Shared.GetApi(url: APIAddress.teacherFacultyDesig , Target: self.view, showLoader: false, completionResponse: { (response) in
                   
                    Commands.println(object: response)
                              do
                               {
                                   let jsonData = try JSONSerialization.data(withJSONObject: response, options: .prettyPrinted)
                                   let model = try JSONDecoder().decode(TeacherFacultyModel.self, from: jsonData)
                                   self.viewDelegate?.getFacultyDesigList(result: model)
                   
                               }
                               catch
                               {
                                 //  self.view.showToastSwift(alrtType: .error, msg: kResponseNotCorrect, title: kOops)
                               }
                   
               }) { (error) in
                 //  self.view.showToastSwift(alrtType: .error, msg: error, title: kOops)
               }
    }
    
    
}

