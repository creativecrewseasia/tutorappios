//
//  SignupModel.swift
//  DoctorApp
//
//  Created by Mohit Sharma on 8/19/20.
//  Copyright © 2020 Cerebrum Infotech. All rights reserved.
//

import Foundation
import ObjectMapper


struct SignupResponseModel: Decodable
{
    let code: Int?
    let success: Bool?
    let message: String?
    let data: DataSignup
}

struct DataSignup: Decodable
{
    let userName, userID, authToken: String?
    let type: Int?

    enum CodingKeys: String, CodingKey {
        case userName
        case userID = "userId"
        case authToken, type
    }
    
}

// MARK: - FacultyDesignation
struct TeacherFacultyModel: Codable {
    let code: Int?
    let success: Bool?
    let message: String?
    let data: DataClass?
}

// MARK: - DataClass
struct DataClass: Codable {
    let faculty, designation, subject: [Designation]?
}

// MARK: - Designation
struct Designation: Codable {
    let id, name: String?
}
