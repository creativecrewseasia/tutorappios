//
//  SignupVC.swift
//  DoctorApp
//
//  Created by Mohit Sharma on 8/18/20.
//  Copyright © 2020 Cerebrum Infotech. All rights reserved.
//
//aa
import UIKit
import ValidationTextField
import DropDown
//import RSSelectionMenu

class SignupVC: UIViewController
{
    
    @IBOutlet var tfUserName: ValidationTextField!
    @IBOutlet var tfEmail: ValidationTextField!
    @IBOutlet var tfPassword: ValidationTextField!
    @IBOutlet var tfCPassword: ValidationTextField!
    
    @IBOutlet weak var textLastName: ValidationTextField!
    @IBOutlet var ivDoctor: UIImageView!
    @IBOutlet var ivPatient: UIImageView!
    
    @IBOutlet var btnDoc: UIButton!
    @IBOutlet var btnPatient: UIButton!
    @IBOutlet var btnSignup: ButtonWithShadowAndRadious!
    
    @IBOutlet weak var btnQues: CustomButton!
    @IBOutlet weak var viewStudent: UIView!
    @IBOutlet weak var btnPlan: CustomButton!
    @IBOutlet weak var kStudentViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var textFavSuperHero: UITextField!
    @IBOutlet weak var textHelp: UITextField!
    @IBOutlet weak var textNeeded: UITextField!
    @IBOutlet weak var textGrade: UITextField!
    @IBOutlet weak var textInfo: UITextField!
    @IBOutlet weak var btnSubQues: CustomButton!
    @IBOutlet weak var btnCancelQues: CustomButton!
    
    @IBOutlet weak var viewTeacher: UIView!
    @IBOutlet weak var textSubject: UITextField!
    @IBOutlet weak var textDesignation: UITextField!
    @IBOutlet weak var textFacu: UITextField!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgBack: UIImageView!
    @IBOutlet weak var viewQues: CustomUIView!
    
    @IBOutlet weak var btnGrade: UIButton!
    @IBOutlet weak var btnAge: UIButton!
    @IBOutlet weak var btnHelp: UIButton!
    @IBOutlet weak var btnNeed: UIButton!
    
    var viewModel:SignupVC_ViewModel?
    var SubjectIndx = [Int]()
    var selectedSubIds = [String]()
    /// Cell Selection Style
   // var cellSelectionStyle: CellSelectionStyle = .tickmark
    var facultyIndex = 0
    var designationIndex = 0
    var subjectData : [Class]?
    var facultyData = [Designation]()
    var designationData = [Designation]()
    let dropDown = DropDown()
    let multipledropDown = DropDown()
    let agedropDown = DropDown()
    let dispatchGroup = DispatchGroup()
    var simpleSelectedArray = [String]()
    var str = ""
    var newStr = ""
    var type = 2
    static var planId = ""
    static var ammount = ""
    var validDic = ["userName":false,"lastName":false,"email":false,"password": false,"cPassword": false]
    
    var isValid: Bool?
    {
        didSet
        {
            btnSignup.isEnabled = isValid ?? false
            btnSignup.backgroundColor = isValid ?? false ? Appcolor.kYellowTheme : .lightGray
            btnSignup.isUserInteractionEnabled = isValid ?? true ? true : false
            
        }
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        validateEntries()
        setView()
        self.viewStudent.isHidden = false
        self.viewTeacher.isHidden = true
    }
    
    //MARK:- Other Functions
    func setView(){
        self.viewModel = SignupVC_ViewModel.init(view: self, delegate: self)
        viewStudent.isHidden = true
        //kStudentViewHeight.constant = 0
        
        //setColor
        btnCancelQues.backgroundColor = Appcolor.kYellowTheme
        btnSubQues.backgroundColor = Appcolor.kYellowTheme
        btnQues.backgroundColor = Appcolor.kYellowTheme
        btnPlan.backgroundColor = Appcolor.kYellowTheme
        lblTitle.textColor = Appcolor.kTheme_Color
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            //            if self.str == "subject" {
            //                self.SubjectIndx = index
            //                self.textSubject.text = item
            //            }
            if(self.str == "faculty"){
                self.textFacu.text = item
                self.facultyIndex = index
            }
            else{
                self.textDesignation.text = item
                self.designationIndex = index
            }
            
            
        }
        
        agedropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
           
            if self.newStr == "age"{
                self.textInfo.text = item
            }else if self.newStr == "Grade"{
                self.textGrade.text = item
            }else if self.newStr == "need"{
                self.textNeeded.text = item
            }else if self.newStr == "Help"{
                 self.textHelp.text = item
            }else if self.newStr == "Super"{
                self.textFavSuperHero.text = item
            }
        }
        
         agedropDown.selectionBackgroundColor = Appcolor.kTheme_Color
        agedropDown.separatorColor = UIColor.white
        agedropDown.selectedTextColor = UIColor.white
       
        
        dropDown.selectionBackgroundColor = Appcolor.kTheme_Color
        
        multipledropDown.multiSelectionAction = { [weak self] (indices, items) in
            print("Muti selection action called with: \(items)")
            if items.isEmpty {
                self!.textSubject.text = ""
            }
            else{
                self?.SubjectIndx = indices
                self!.textSubject.text = "\(items)"
            }
        }
        multipledropDown.selectionBackgroundColor = Appcolor.kTheme_Color
        multipledropDown.separatorColor = UIColor.white
        multipledropDown.selectedTextColor = UIColor.white
       
        
        DropDown.startListeningToKeyboard()
        
        dispatchGroup.enter()
        self.viewModel?.getSubjectList()
        dispatchGroup.enter()
        self.viewModel?.getFacultyDesigList()
        dispatchGroup.notify(queue: .main) {
            print("Done")
        }
        
    }
    
    //MARK:- subjectPopup
//    func setSubjectPopup(data:[Class]){
//        var aa = [String]()
//        for (_, value) in data.enumerated() {
//            aa.append(value.name)
//        }
//
//        let selectionMenu = RSSelectionMenu(selectionStyle: .multiple, dataSource: aa) { (cell, name, indexPath) in
//            cell.textLabel?.text = name
//            // set image
//            // cell.imageView?.image = #imageLiteral(resourceName: "profile")
//            cell.tintColor = Appcolor.kTheme_Color
//        }
//        //                       // add first row as 'All'
//        //                let allSelected = (simpleSelectedArray.count == 0)
//        //
//        //                selectionMenu.addFirstRowAs(rowType: .all, showSelected: allSelected) { (text, selected) in
//        //
//        //                    if selected {
//        //
//        //                        /// do some stuff...
//        //                        print("All option selected")
//        //                    }
//        //                }
//
//        // cell selection style
//        selectionMenu.cellSelectionStyle = self.cellSelectionStyle
//        // selected items and delegate
//        selectionMenu.setSelectedItems(items: simpleSelectedArray) { (name, index, selected, selectedItems) in
//            print(index)
//            if(selected == true){
//                self.SubjectIndx.append(index)
//            }
//            else{
//                if self.SubjectIndx.count > 0{
//                    self.SubjectIndx.remove(at: index)
//                }
//            }
//        }
//
//        // on dismiss handler
//        selectionMenu.onDismiss = { [weak self] items in
//            self?.simpleSelectedArray = items
//            /// do some stuff on menu dismiss
//            let displayText = items.joined(separator: ", ")
//            self?.textSubject.text = displayText.isEmpty ? "" : displayText
//            print(self?.SubjectIndx)
//        }
//        
//        // show menu
//        selectionMenu.show(style: .actionSheet(title: "Select Subject", action: "Done", height: 200), from: self)
//    }
    
    //MARK:- Action
    
    @IBAction func actnAge(_ sender: Any) {
        newStr = "age"
        agedropDown.dataSource = ["4 - 1","10 - 15","15 - 20"]
        agedropDown.anchorView = self.textInfo
        agedropDown.show()
    }
    
    @IBAction func actnGrade(_ sender: Any) {
        newStr = "Grade"
        agedropDown.dataSource = ["First","Second","Third","Fourth","Fifth","Sixth","Seventh","Eighth ","Ninth","Tenth","Eleventh","Twelfth"]
        agedropDown.anchorView = self.textGrade
        agedropDown.show()
        
    }
    
    @IBAction func actnNeed(_ sender: Any) {
        newStr = "need"
        agedropDown.dataSource = ["Help in homework","Subject help","Extra Learning"]
        agedropDown.anchorView = self.textGrade
        agedropDown.show()
        
    }
    
    @IBAction func actnHelp(_ sender: Any) {
      newStr = "Help"
        agedropDown.dataSource = ["New learner","Struggling with subject","Give specific instructions"]
        agedropDown.anchorView = self.textHelp
        agedropDown.show()
        
    }
    
    @IBAction func actnSuperHero(_ sender: Any) {
        newStr = "Super"
        agedropDown.dataSource = ["Invisibility","Can fly","Super-Fast Running","Super Strong","Control the Weather","Read Minds","Walk thru walls","Super hearing","Super eyesight","Mega Swimming"]
        agedropDown.anchorView = self.textFavSuperHero
        agedropDown.show()
    }
    
    
    @IBAction func selectDesignation(_ sender: Any) {
        str = "desi"
        if (designationData.count > 0){
            var aa = [String]()
            for (_, value) in designationData.enumerated() {
                aa.append(value.name ?? "")
            }
            dropDown.dataSource = aa
            dropDown.anchorView = self.textDesignation
            dropDown.show()
        }
        else{
            showToastSwift(alrtType: .info, msg: "No Designation found!", title: "")
        }
    }
    @IBAction func selectFaculty(_ sender: Any) {
        str = "faculty"
        if (facultyData.count > 0){
            var aa = [String]()
            for (_, value) in facultyData.enumerated() {
                aa.append(value.name ?? "")
            }
            dropDown.dataSource = aa
            dropDown.anchorView = self.textFacu
            dropDown.show()
        }
        else{
            showToastSwift(alrtType: .info, msg: "No Faculty found!", title: "")
        }
    }
    
    @IBAction func selectSubjectAction(_ sender: Any) {
        str = "subject"
        if let subjectList = subjectData{
            if (subjectList.count > 0){
                // setSubjectPopup(data:subjectList)
                var aa = [String]()
                for (_, value) in subjectList.enumerated() {
                    aa.append(value.name ?? "")
                }
                multipledropDown.dataSource = aa
                multipledropDown.anchorView = self.textSubject
                multipledropDown.show()
                
            }
            else{
                showToastSwift(alrtType: .info, msg: "No subject found!", title: "")
            }
        }
    }
    
    @IBAction func submitQues(_ sender: Any) {
        viewQues.isHidden = true
        imgBack.isHidden = true
        
    }
    @IBAction func cancelQues(_ sender: Any) {
        viewQues.isHidden = true
        imgBack.isHidden = true
        textInfo.text = ""
        textGrade.text = ""
        textHelp.text = ""
        textNeeded.text = ""
        textNeeded.text = ""
    }
    @IBAction func QuesAction(_ sender: Any) {
        viewQues.isHidden = false
        imgBack.isHidden = false
    }
    @IBAction func planAction(_ sender: Any) {
        let controller = Navigation.GetInstance(of: .AddTokenListVC)as! AddTokenListVC
        self.push_To_Controller(from_controller: self, to_Controller: controller)
        
    }
    @IBAction func acnDoc(_ sender: UIButton)
    {
        //Teacher
        self.ivDoctor.image = UIImage(named: "radio_selected")
        self.ivPatient.image = UIImage(named: "radio_unselected")
        type = 2
        viewStudent.isHidden = true
        viewTeacher.isHidden = false
        // kStudentViewHeight.constant = 0
    }
    @IBAction func acnPatient(_ sender: UIButton)
    {
        //Student
        self.ivDoctor.image = UIImage(named: "radio_unselected")
        self.ivPatient.image = UIImage(named: "radio_selected")
        type = 1
        viewStudent.isHidden = false
        viewTeacher.isHidden = true
        // kStudentViewHeight.constant = 118
    }
    
   
    @IBAction func acnLogin(_ sender: Any)
    {
        self.moveBACK(controller: self)
    }
    
    @IBAction func acnSignup(_ sender: Any)
    {
        self.view.endEditing(true)
        if (type == 2)
        {
            if (textFacu.text == ""){
                showToastSwift(alrtType: .statusOrange, msg: "Please select faculty", title: "")
            }
            else if (textDesignation.text == ""){
                showToastSwift(alrtType: .statusOrange, msg: "Please select designation", title: "")
            }
            else if(textSubject.text == ""){
                showToastSwift(alrtType: .statusOrange, msg: "Please select subject", title: "")
            }
            else{
                selectedSubIds.removeAll()
                for index in SubjectIndx{
                selectedSubIds.append(self.subjectData?[index].id ?? "")//add(self.subjectData?[index].id ?? "")/
                }
                
                viewModel?.SignupMentor()
            }
        }
        else
        {
            if SignupVC.planId == "" {
                showToastSwift(alrtType: .statusOrange, msg: "Please select plan", title: "")
            }
            else{
                viewModel?.SignupStudent()
            }
        }
    }
    
    
    func validateEntries()
    {
        tfEmail.validCondition = {$0.count > 5 && $0.contains("@") && $0.contains(".")}
        tfUserName.validCondition = {$0.count > 3}
        textLastName.validCondition = {$0.count > 3}
        tfPassword.validCondition = {$0.count > 5}
        tfCPassword.validCondition = {$0.count > 5}
        tfCPassword.validCondition =
            {
                guard let password = self.tfPassword.text else
                {
                    return false
                }
                return $0 == password
        }
        
        tfUserName.successImage = UIImage(named: "success")
        tfUserName.errorImage = UIImage(named: "error")
        
        textLastName.successImage = UIImage(named: "success")
        textLastName.errorImage = UIImage(named: "error")
        
        tfEmail.successImage = UIImage(named: "success")
        tfEmail.errorImage = UIImage(named: "error")
        
        tfPassword.successImage = UIImage(named: "success")
        tfPassword.errorImage = UIImage(named: "error")
        
        tfCPassword.successImage = UIImage(named: "success")
        tfCPassword.errorImage = UIImage(named: "error")
        
        tfEmail.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        tfPassword.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        tfUserName.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        textLastName.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        tfCPassword.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        self.btnSignup.backgroundColor = Appcolor.kTextColorGray
        btnSignup.isUserInteractionEnabled = false
        self.btnSignup.updateLayerProperties()
    }
    
    
    @objc func textFieldDidChange(_ textfield: UITextField)
    {
        let tf = textfield as! ValidationTextField
        
        switch tf
        {
        case tfEmail:
            validDic["userName"] = tf.isValid
        case textLastName:
            validDic["lastName"] = tf.isValid
        case tfUserName:
            validDic["email"] = tf.isValid
        case tfPassword:
            validDic["password"] = tf.isValid
        case tfCPassword:
            validDic["cPassword"] = tf.isValid
        default:
            break
        }
        
        isValid = validDic.reduce(true){ $0 && $1.value}
    }
    
}

//MARK:- TextFeild Delegate
extension SignupVC:UITextFieldDelegate
{
    func textFieldShouldClear(_ textField: UITextField) -> Bool
    {
        return true;
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if(textField == textInfo){
            textGrade.becomeFirstResponder()
        }
        if(textField == textGrade){
            textNeeded.becomeFirstResponder()
        }
        if(textField == textNeeded)
        {
            textHelp.becomeFirstResponder()
        }
        if(textField == textHelp)
        {
            textFavSuperHero.becomeFirstResponder()
        }
        if(textField == textFavSuperHero){
            textField.resignFirstResponder()
        }
        return true;
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if (string == " ") && (textField.text?.count)! == 0
        {
            return false
        }
        return true
    }
}

//MARK:- ViewDelegate

extension SignupVC:SignupDelegate{
    func getFacultyDesigList(result: TeacherFacultyModel) {
        if let data = result.data?.faculty{
            if data.count > 0{
                facultyData = data
            }
        }
        if let data = result.data?.designation{
            if data.count > 0{
                designationData = data
            }
        }
    }
    
    func getSubjectList(result: subjectData) {
        if result.data.subject.count > 0 {
            subjectData = result.data.subject
        }
    }
    
    
}
