//
//  TeacherSignupVC.swift
//  DoctorApp
//
//  Created by Poonam  on 31/08/20.
//  Copyright © 2020 Cerebrum Infotech. All rights reserved.
//

import UIKit
import DropDown

class TeacherSignupVC: CustomController {

    @IBOutlet weak var txtFildFirstName: UITextField!
    @IBOutlet weak var txtFildLastName: UITextField!
    
    @IBOutlet weak var btnGrade: UIButton!
    @IBOutlet weak var txtFildGrade: UITextField!
    @IBOutlet weak var txtFildAddress: UITextField!
    
    @IBOutlet weak var txtFildCV: UITextField!
    @IBOutlet weak var txtFildBankName: UITextField!
    @IBOutlet weak var btnSubject: UIButton!
    @IBOutlet weak var txtFildSubject: UITextField!
    @IBOutlet weak var txtFildHours: UITextField!
    @IBOutlet weak var txtFildPasswrd: UITextField!
    @IBOutlet weak var txtFildEmail: UITextField!
    @IBOutlet weak var txtFildEducation: UITextField!
    
    @IBOutlet weak var textFldPhnNum: UITextField!
    @IBOutlet weak var txtFildCerti: UITextField!
    @IBOutlet weak var txtFildReport: UITextField!
    @IBOutlet weak var btnCV: UIButton!
    @IBOutlet weak var btnReport: UIButton!
    @IBOutlet weak var btnCertificates: UIButton!
    @IBOutlet weak var btnLicense: UIButton!
    @IBOutlet weak var txtFildDriverse: UITextField!
    @IBOutlet weak var txtFildAccountNumber: UITextField!
    @IBOutlet weak var txtFildIFC: UITextField!
    @IBOutlet weak var txtFildUserName: UITextField!
    
    @IBOutlet weak var btn18: UIButton!
    
    @IBOutlet weak var btnCharged: UIButton!
    @IBOutlet weak var btnArrested: UIButton!
    
//    let pickerController = UIImagePickerDelegate()
    var viewModel:TeacherSignUpViewModel?
    var uploadData = NSMutableArray()
    var booledit = false
    var isUploadFrom = 0
    let dropDown = DropDown()
    let dropDown2 = DropDown()
     var SubjectIndx = [Int]()
    var gradeStr = [String]()
//    var SubjectIndx = [String]()
    var str = ""
    var subjectData : [Class]?
    var certiBase64 : URL?
    var reportBase64 : URL?
    var cvBase64 : URL?
    var imgUrl : URL?
    var arrested = "1"
    var Charged = "1"
    var is18 = "No"
    //MARK: - UIImagePickerController
    private var imagePicker =  UIImagePickerController()
    override func viewDidLoad() {
        super.viewDidLoad()
        setView()
        imagePicker.delegate = self
     //   UIImagePickerDelegate = self
//        pickerController.delegate = self
         btn18.isSelected = false
        dropDown.multiSelectionAction = { [weak self] (indices, items) in
                   print("Muti selection action called with: \(items)")
                
                       //self?.exerciseNameTextField.setTitle("", for: .normal)
            if self?.str == "subject" {
                self?.txtFildSubject.text = "\(items)"
                self?.SubjectIndx = indices
            }else{
                self?.txtFildGrade.text = "\(items)"
                self?.gradeStr = items
            }
        }
        
        
        dropDown2.selectionAction = { [unowned self] (index: Int, item: String) in
                print("Selected item: \(item) at index: \(index)")
                  if self.str == "Hours" {
                     self.txtFildHours.text = item
                  }
              }
              DropDown.startListeningToKeyboard()
        
          self.viewModel?.getSubjectList()
    }
    func setView(){
    self.viewModel = TeacherSignUpViewModel.init(view: self, delegate: self)
    }
    
    @IBAction func actnSignup(_ sender: Any) {
        self.viewModel?.SignupTeacher()
        
    }
    
    @IBAction func actnAvailableHour(_ sender: Any) {
        str = "Hours"
        dropDown2.dataSource = ["08:00 AM - 05:00 PM","09:00 AM - 06:00 PM","10:00 AM - 07:00 PM","11:00 AM - 08:00 PM","12:00 PM - 09:00 PM"]
        dropDown2.anchorView = self.txtFildHours
        dropDown2.show()
    }
    
    @IBAction func actnGrade(_ sender: Any) {
          str = "Grade"
        dropDown.dataSource = ["First","Second","Third","Fourth","Fifth","Sixth","Seventh","Eighth ","Ninth","Tenth","Eleventh","Twelfth"]
        dropDown.anchorView = self.txtFildGrade
        dropDown.show()
    }
    
    @IBAction func actnSubject(_ sender: Any) {
       str = "subject"
        if let subjectList = subjectData{
            if (subjectList.count > 0){
                // setSubjectPopup(data:subjectList)
                var aa = [String]()
                for (_, value) in subjectList.enumerated() {
                    aa.append(value.name ?? "")
                }
                dropDown.dataSource = aa
                dropDown.anchorView = self.txtFildSubject
                dropDown.show()
                
            }
            else{
                showToastSwift(alrtType: .info, msg: "No subject found!", title: "")
            }
        }
    }
    
    @IBAction func actnCertificates(_ sender: Any) {
        uploadData.removeAllObjects()
        isUploadFrom = 1
        let importMenu = UIDocumentPickerViewController(documentTypes: ["public.data", "public.content"], in: UIDocumentPickerMode.import)
                     importMenu.delegate = self
              present(importMenu, animated: true, completion: nil)
    }
    
    @IBAction func actnDriverseUpload(_ sender: Any) {
        self.addImageOptions()
    }
    
    @IBAction func actnReportUpload(_ sender: Any) {
         uploadData.removeAllObjects()
        isUploadFrom = 2
        let importMenu = UIDocumentPickerViewController(documentTypes: ["public.data", "public.content"], in: UIDocumentPickerMode.import)
                     importMenu.delegate = self
              present(importMenu, animated: true, completion: nil)
    }
    
    @IBAction func actnCVUpload(_ sender: Any) {
         uploadData.removeAllObjects()
        isUploadFrom = 3
        let importMenu = UIDocumentPickerViewController(documentTypes: ["public.data", "public.content"], in: UIDocumentPickerMode.import)
                     importMenu.delegate = self
              present(importMenu, animated: true, completion: nil)
    }
    
    @IBAction func actn18(_ sender: Any) {
            is18 = "yes"
            btn18.isSelected == true
       
    }
    
    @IBAction func actnArrested(_ sender: Any) {
            arrested = "0"
            btnArrested.isSelected == true
        
    }
    
    @IBAction func actnCharged(_ sender: Any) {
            Charged = "0"
            btnCharged.isSelected == true
    }
    
}
extension TeacherSignupVC: UIDocumentMenuDelegate,UIDocumentPickerDelegate{
    
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        guard let myURL = urls.first else {
            return
        }
        booledit = false
        if uploadData.count <= 1 {
            
            if uploadData.count > 0 {
            
            _ = uploadData.enumerated().map { (index,element) in
            
            let dd = element as? [String:Any]
            if dd?["fileName"] as? String == myURL.lastPathComponent {
//                self.showAlert(Message: "You have already selected this file.")
                booledit = true
                return
            }
                }
                if booledit == false {
          
                var modelHW = [String: Any]()
                      modelHW["url"] = myURL
                      modelHW["fileName"] = myURL.lastPathComponent
                      modelHW["id"] = 0
                      uploadData.add(modelHW)
                }
        }
        else{
                var modelHW = [String: Any]()
                modelHW["url"] = myURL
                modelHW["fileName"] = myURL.lastPathComponent
                modelHW["id"] = 0
                uploadData.add(modelHW)
                if isUploadFrom == 1{
                    txtFildCerti.text = myURL.lastPathComponent
                    certiBase64 = myURL
                    print("certiBase64: ",certiBase64)
                }else if isUploadFrom == 2{
                    txtFildReport.text = myURL.lastPathComponent
                     reportBase64 = myURL
                }else if isUploadFrom == 3{
                    txtFildCV.text = myURL.lastPathComponent
                    cvBase64 = myURL
                }
            }}
        else{
//            self.showAlert(Message: "Maximum limit to upload the document is 5.")
        }
    }
    
    
    public func documentMenu(_ documentMenu:UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
    }
    
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("view was cancelled")
        dismiss(animated: true, completion: nil)
    }
}
//MARK:- ViewDelegate

extension TeacherSignupVC:TeacherSignupDelegate{
  
    
    func getSubjectList(result: subjectData) {
        if result.data.subject.count > 0 {
            subjectData = result.data.subject
        }
    }
    
    
}

extension TeacherSignupVC:UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
         print(info)
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
                let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
                // choose a name for your image
                let fileName = "/\(Double(Date.timeIntervalSinceReferenceDate * 1000)).jpg"
                // create the destination file url to save your image
                let fileURL = documentsDirectory.appendingPathComponent(fileName)
                // get your UIImage jpeg data representation and check if the destination file url already exists
                if let data = pickedImage.jpegData(compressionQuality: 1.0),
                    !FileManager.default.fileExists(atPath: fileURL.path) {
                    do {
                        // writes the image data to disk
                        try data.write(to: fileURL)
                        if #available(iOS 11.0, *) {
                            if let url = info[UIImagePickerController.InfoKey.imageURL] as? URL {
                                self.txtFildDriverse.text = url.lastPathComponent
                                self.imgUrl = fileURL
                            }
                        } else {
                             self.txtFildDriverse.text = fileName
                            // Fallback on earlier versions
                        }
                        //print("file saved")
                    } catch {
                        //print("error saving file:", error)
                    }
                }
        }else{
          
           }
       
      
        dismiss(animated: true)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true)
    }
}
