//
//  AddCreditCell.swift
//  DoctorApp
//
//  Created by Poonam  on 14/09/20.
//  Copyright © 2020 Cerebrum Infotech. All rights reserved.
//


import Foundation
import UIKit

class AddCreditCell: UICollectionViewCell {

    @IBOutlet weak var imagCredit: UIImageView!
    
    @IBOutlet weak var creditName: UILabel!
    
    @IBOutlet weak var creditDescription: UILabel!
    
    @IBOutlet weak var btnCredit: UIButton!
    @IBOutlet weak var imgSeleted: UIImageView!
    
   
}
