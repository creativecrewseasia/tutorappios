//
//  AddMeetingVC.swift
//  DoctorApp
//
//  Created by Atinder Kaur on 8/25/20.
//  Copyright © 2020 Cerebrum Infotech. All rights reserved.
//

import UIKit
import DropDown

class AddMeetingVC: CustomController {
    let dropDown = DropDown()
   
    @IBOutlet weak var bookNowConsraintHeight: NSLayoutConstraint!
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var txtfieldSlot: UITextField!
    @IBOutlet weak var txtfieldDate: UITextField!
    @IBOutlet weak var txtfieldSub: UITextField!
    @IBOutlet weak var btnAddBooking: ButtonWithShadowAndRadious!
    var str = ""
    var SubjectIndx = 0
    var subjectData : [Class]?
    var viewModel:AddMeetingViewModel?
    var subject : String?
    var date : String?
    var slot : String?
    var isEdit : Bool?
    var subjectID : String?
    var creditVal = "0"
    var bookedData = [[String : Any]]()
    
 var creditName = ["Super Learning","Mega Brain Power","Project X"]
 var creditDescription = ["30 minutes at just 30 credits.Suggested for K - 6th grade","45 minutes at just 45 credits.Suggested for 4th - 12th grade","60 minutes at just 60 credits.Suggested for 6th - 12th grade"]
var creditImg = ["Credit1","Credit2","Credit3"]
var isSelect = ["0","0","0"]
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isEdit == true{
            setData()
            bookNowConsraintHeight.constant = 0
        }else{
            bookNowConsraintHeight.constant = 50
        }
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
          print("Selected item: \(item) at index: \(index)")
            if self.str == "subject" {
            self.SubjectIndx = index
            self.txtfieldSub.text = item
            }
            else{
                self.txtfieldSlot.text = item
            }
        }
        DropDown.startListeningToKeyboard()
        setUI()
        // Do any additional setup after loading the view.
    }
    func setData(){
        txtfieldSub.text = subject
        txtfieldDate.text = date
        txtfieldSlot.text = slot
       
    }
    func setUI() {
        self.title = "Add Multiple Booking"
        btnAddBooking.updateLayerProperties()
        self.viewModel = AddMeetingViewModel.init(view: self, delegate: self)
         setDatePickerView(self.view, type: .date)
    }
    
    @IBAction func actionSubjectBtn(_ sender: Any) {
        str = "subject"
        self.viewModel?.getSubjectList()
    }
    

    @IBAction func actionSelectSubject(_ sender: UIButton) {
    }

    @IBAction func actionSelectDate(_ sender: UIButton) {
       showDatePicker(datePickerDelegate: self)
    }

    @IBAction func actionTimeSlot(_ sender: UIButton) {
        if subjectData?.count ?? 0 > 0 {
        str = "slot"
            var param = [String : Any]()
        param["subjectId"] = subjectData?[SubjectIndx].id
            param["date"] = txtfieldDate.text
        self.viewModel?.getSlots(Params: param)
        }
    }
    
    @IBAction func actionCredit(_ sender: Any) {
        if (sender as AnyObject).tag == 0{
            creditVal = "30"
        }else  if (sender as AnyObject).tag == 1{
             creditVal = "45"
        }else  if (sender as AnyObject).tag == 2{
             creditVal = "60"
        }
        for i in 0..<isSelect.count{
            if i == (sender as AnyObject).tag {
                isSelect[i] = "1"
            }else{
                isSelect[i] = "0"
            }
        }
         collectionView.reloadData()
        
    }
    
//
    @IBAction func actionAddBooking(_ sender: UIButton) {
        if txtfieldSub.text == "" {
            self.showToastSwift(alrtType: .error, msg: "Please select subject", title: kOops)
        }
        else if txtfieldDate.text == "" {
            self.showToastSwift(alrtType: .error, msg: "Please select date", title: kOops)
        }
        else if txtfieldSlot.text == "" {
            self.showToastSwift(alrtType: .error, msg: "Please select slot", title: kOops)
        }
        else{
           var data = [String:Any]()
            data = ["time":txtfieldSlot.text , "date": txtfieldDate.text,"subjectId":subjectData?[SubjectIndx].id, "credit": creditVal]
            print("data: ",data)
            bookedData.append(data)
            self.txtfieldSub.text = ""
            self.txtfieldDate.text = ""
            self.txtfieldSlot.text = ""
        }
        
        

    }
    
    @IBAction func actionBookNow(_ sender: Any) {
          var param = [String : Any]()
        param["userId"] = AppDefaults.shared.userID
        param["sessions"] = self.bookedData
        self.viewModel?.addBooking(Params: param)
        
    }
    //MARK:-For Convert Date Into String dd/mm/yyyy
      func convertDateIntoStringWithDDMMYYYY(date: Date) -> String{
          let dateFormatter = DateFormatter()
          // initially set the format based on your datepicker date / server String
          dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
          //Date which you need to convert in string
          let dateString = dateFormatter.string(from: date)
          let date = dateFormatter.date(from: dateString)
          //then again set the date format whhich type of output you need
          dateFormatter.dateFormat = "yyyy-MM-dd"
          dateFormatter.locale = Locale.current
          // again convert your date to string
          if date != nil{
              let strDate = dateFormatter.string(from: date!)
              return strDate
          }
          return ""
      }
    
}
extension AddMeetingVC:SharedUIDatePickerDelegate{
    
    func doneButtonClicked(datePicker: UIDatePicker) {
        let strDate = convertDateIntoStringWithDDMMYYYY(date: datePicker.date)
        txtfieldDate.text = strDate
    }
}

extension AddMeetingVC : MeetingDelegate {
   func getSubjectList(result: subjectData) {
    if result.data.subject.count > 0 {
        
        var aa = [String]()
        subjectData = result.data.subject
        for (_, value) in result.data.subject.enumerated() {
            aa.append(value.name)
        }
        
         dropDown.dataSource = aa
        dropDown.anchorView = self.txtfieldSub
        dropDown.show()
    }
    else{
        showToastSwift(alrtType: .error, msg: result.message , title: kOops)
    }
     }
     
     func getTimeSlot(slots: Slots) {
        
        if slots.data?.count  ?? 0 > 0 {
            
            dropDown.dataSource = slots.data!
            dropDown.anchorView = self.txtfieldSlot
            dropDown.show()
        }
        else{
         
            showToastSwift(alrtType: .error, msg: slots.message ?? "", title: kOops)
        }
         
     }
     
}


//MARK:- UICollectionViewDelegate UICollectionViewDelegate
extension AddMeetingVC : UICollectionViewDelegate , UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
   
        return creditName.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as! AddCreditCell
        cell.creditName.text = creditName[indexPath.row]
        cell.creditDescription.text = creditDescription[indexPath.row]
        cell.imagCredit.image = UIImage(named: creditImg[indexPath.row])
        cell.btnCredit.tag = indexPath.row
        if isSelect[indexPath.row] == "0"{
            cell.imgSeleted.image = UIImage(named: "")
        }else{
          
                cell.imgSeleted.image = UIImage(named: "Select")
          
        }
     
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
//        collectionView.reloadData()
    }
}
