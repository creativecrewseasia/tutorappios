//
//  DashBoardVC.swift
//  DoctorApp
//
//  Created by Atinder Kaur on 8/25/20.
//  Copyright © 2020 Cerebrum Infotech. All rights reserved.
//

import UIKit

class DashBoardVC: CustomController {

    @IBOutlet weak var customSegment: UISegmentedControl!
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var btnMenu: UIBarButtonItem!
    @IBOutlet weak var tblViewListing: UITableView!
    var viewModel:DashboardVC_ViewModel?
    var dataList : [SubList]?
    var refreshControl = UIRefreshControl()

    override func viewDidLoad() {
        super.viewDidLoad()
        setView()
    }
    
    func setView() {
        self.viewModel = DashboardVC_ViewModel.init(view: self, delegate: self)
        CornerRadius(radius: 30, view: addBtn)
        addBtn.layer.borderColor = Appcolor.kTheme_Color.cgColor
        addBtn.layer.borderWidth = 1
        tblViewListing.tableFooterView = UIView()
        tblViewListing.separatorColor = UIColor.clear
        activeData()
        customSegment.selectedSegmentIndex = 0
        if #available(iOS 13.0, *) {
            customSegment.selectedSegmentTintColor = UIColor.white
        } else {
            // Fallback on earlier versions
        }
        addBtn.frame = CGRect(x: self.view.frame.origin.x +  self.view.frame.size.width - 80, y: self.view.frame.origin.y +  self.view.frame.size.height - 140, width: 60, height: 60)
        refreshControl.attributedTitle = NSAttributedString(string: "Loading")
               refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        tblViewListing.addSubview(refreshControl)
        var param = [String : Any]()
        if AppDefaults.shared.firebaseToken != "" {
          param["deviceToken"] = AppDefaults.shared.firebaseToken
           param["userId"] = AppDefaults.shared.userID
           self.viewModel?.updateDeviceToken(Params: param)
        }
    }
    
    @objc func refresh(_ sender: AnyObject) {
              if AllUtilies.isConnectedToInternet
                     {
                         if (customSegment.selectedSegmentIndex == 0)
                                       {
                                           self.activeData()
                                       }
                                       else
                                       {
                                           self.historyData()
                                       }
             }
                     else
                     {
                         self.showToastSwift(alrtType:.info, msg: "No internet connection.", title:kOops)
                     }
       }
    
    func activeData() {
      self.viewModel?.getActiveClasses()
    }
    
    func historyData() {
         self.viewModel?.getHistoryClasses()
    }
    
  //Button Actions
    @IBAction func actionSideMenu(_ sender: UIButton) {
       self.revealViewController()?.revealToggle(animated: true)
    }
   
    @IBAction func actionNotification(_ sender: UIButton) {
        
        showAlertMessage(titleStr: kAppName, messageStr: "Coming Soon")
        
    }
    
    @IBAction func actionAdd(_ sender: UIButton) {
        if AllUtilies.isConnectedToInternet
                  {
                     let storyBoard: UIStoryboard = UIStoryboard(name: "Student2", bundle: nil)
                     let newViewController = storyBoard.instantiateViewController(withIdentifier: "AddMeetingVC") as! AddMeetingVC
                          newViewController.modalPresentationStyle = .fullScreen
                     newViewController.isEdit = false
                     self.navigationController?.pushViewController(newViewController, animated: true)
          }
                  else
                  {
                      self.showToastSwift(alrtType:.info, msg: "No internet connection.", title:kOops)
                  }
         
    }
    
    @IBAction func joinVideoCall(_ sender: UIButton) {
        let controller = Navigation.GetInstance(of: .VideoChatViewController)as! VideoChatViewController
        controller.channelName = dataList?[sender.tag].channelName
        controller.accessToken = dataList?[sender.tag].accessToken
        self.push_To_Controller(from_controller: self, to_Controller: controller)
        
    }
    
    @IBAction func actnCancelBooking(_ sender: Any) {
        var bookingID = dataList?[(sender as AnyObject).tag].id
         var param = [String : Any]()
        param["bookId"] = bookingID
        self.viewModel?.cancelBooking(Params : param)
    }
    
    @IBAction func actnReshduleBooking(_ sender: Any) {
        if AllUtilies.isConnectedToInternet
        {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Student2", bundle: nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "AddMeetingVC") as! AddMeetingVC
            newViewController.modalPresentationStyle = .fullScreen
            newViewController.isEdit = true
            newViewController.subject = dataList?[(sender as AnyObject).tag].subjectName
            newViewController.date = dataList?[(sender as AnyObject).tag].bookingDate
             newViewController.slot = dataList?[(sender as AnyObject).tag].timeSlot
            newViewController.subjectID = dataList?[(sender as AnyObject).tag].subjectId
            newViewController.creditVal = dataList?[(sender as AnyObject).tag].credit ?? ""
            self.navigationController?.pushViewController(newViewController, animated: true)
        }
        else
        {
            self.showToastSwift(alrtType:.info, msg: "No internet connection.", title:kOops)
        }
        
    }
    
    @IBAction func valueChangedSegment(_ sender: UISegmentedControl) {
        
        if AllUtilies.isConnectedToInternet
                {
                    if (sender.selectedSegmentIndex == 0)
                                  {
                                      self.activeData()
                                  }
                                  else
                                  {
                                      self.historyData()
                                  }
        }
                else
                {
                    self.showToastSwift(alrtType:.info, msg: "No internet connection.", title:kOops)
                }
    }
}

extension DashBoardVC : DashboardDelegate {
    func addedSuccessfully(result: ListModel) {
         self.refreshControl.endRefreshing()
          dataList?.removeAll()
        if result.data?.count ?? 0 > 0 {
            dataList = result.data
        }
        tblViewListing.reloadData()

    }
    func cancelSuccessfully(result: String){
          tblViewListing.reloadData()
    }
    
    func failed() {
        
    }
}


extension DashBoardVC : UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if dataList?.count  ?? 0 > 0 {
            return (dataList?.count ?? 0)
        }else{
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! DashboardCell
        cell.lblDate.text = dataList?[indexPath.row].bookingDate
        cell.lblTime.text = dataList?[indexPath.row].timeSlot
        cell.btnJoin.tag = indexPath.row
         cell.btnEdit.tag = indexPath.row
          cell.btnCancel.tag = indexPath.row
        
        if  customSegment.selectedSegmentIndex == 1 {
            cell.btnJoin.isHidden = true
        }
        else{
            if (dataList?[indexPath.row].status ?? 0 == 1){
            cell.btnJoin.isHidden = false
            }
            else{
             cell.btnJoin.isHidden = true
            }
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 98
    }
}
