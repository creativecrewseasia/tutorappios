//
//  DashboardCell.swift
//  DoctorApp
//
//  Created by Atinder Kaur on 8/25/20.
//  Copyright © 2020 Cerebrum Infotech. All rights reserved.
//

import UIKit

class DashboardCell: UITableViewCell {
    
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var viewBG: UIView!
    @IBOutlet weak var btnJoin: ButtonWithShadowAndRadious!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setUI()
        
    }
    
    func setUI ()
    {
         viewBG.layer.shadowOffset = CGSize(width: 0, height: 0)
         viewBG.layer.shadowColor = UIColor.black.cgColor
         viewBG.layer.shadowRadius = 5
         viewBG.layer.shadowOpacity = 0.40
         viewBG.layer.masksToBounds = false;
         viewBG.clipsToBounds = false;
         btnJoin.updateLayerProperties()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
