//
//  DashBoardViewModel.swift
//  DoctorApp
//
//  Created by Atinder Kaur on 8/25/20.
//  Copyright © 2020 Cerebrum Infotech. All rights reserved.
//

import Foundation

protocol DashboardDelegate: class {
   
    func addedSuccessfully (result : ListModel)
    func cancelSuccessfully(result: String)
    func failed()
}



class DashboardVC_ViewModel
{
    var view : DashBoardVC
    var localDashboardDelegate : DashboardDelegate?
  
    init(view : DashBoardVC, delegate: DashboardDelegate )
       {
           self.view = view
           localDashboardDelegate = delegate
       }
    
    func getHistoryClasses()
    {

        WebService.Shared.GetApi(url: APIAddress.History_Class + AppDefaults.shared.userID, Target: self.view, showLoader: true, completionResponse: { (response) in
            
             Commands.println(object: response)
            
                        do
                        {
                            let jsonData = try JSONSerialization.data(withJSONObject: response, options: .prettyPrinted)
                            let model = try JSONDecoder().decode(ListModel.self, from: jsonData)
                            
                            self.localDashboardDelegate?.addedSuccessfully(result: model)
                            
            
                        }
                        catch
                        {
                            self.view.showToastSwift(alrtType: .error, msg: kResponseNotCorrect, title: kOops)
                        }
            
        }) { (error) in
            self.view.showToastSwift(alrtType: .error, msg: error, title: kOops)
        }
        
    }
    
    func getActiveClasses() {
       
        WebService.Shared.GetApi(url: APIAddress.Active_Class + AppDefaults.shared.userID, Target: self.view, showLoader: true, completionResponse: { (response) in
            
             Commands.println(object: response)
            
                        do
                        {
                            let jsonData = try JSONSerialization.data(withJSONObject: response, options: .prettyPrinted)
                            let model = try JSONDecoder().decode(ListModel.self, from: jsonData)
                            
                            self.localDashboardDelegate?.addedSuccessfully(result: model)
                            
            
                        }
                        catch
                        {
                            self.view.showToastSwift(alrtType: .error, msg: kResponseNotCorrect, title: kOops)
                        }
            
        }) { (error) in
            self.view.showToastSwift(alrtType: .error, msg: error, title: kOops)
        }
    }
    
    func cancelBooking(Params : [String:Any]){
        WebService.Shared.PostApi(url: APIAddress.cancelBooking, parameter: Params, Target: self.view, showLoader: true, completionResponse: { (response) in
            Commands.println(object: response)
            do
            {
                
                let jsonData = try JSONSerialization.data(withJSONObject: response, options: .prettyPrinted)
                let model = try JSONDecoder().decode(Slots.self, from: jsonData)

                self.localDashboardDelegate?.cancelSuccessfully(result: model.message ?? "")
                
                
            }
            catch
            {
                self.view.showToastSwift(alrtType: .error, msg: kResponseNotCorrect, title: kOops)
            }
            do
            {
                let data = response as? [String : Any]
                if let code = data?["code"] as? Int  {
                    if code == 200 {
                    }
                }
            }
            catch
            {
            }
        }) { (error) in
            
        }
        
    }
    
    func updateDeviceToken(Params : [String:Any]) {
        WebService.Shared.PostApi(url: APIAddress.DeviceToken, parameter: Params, Target: self.view, showLoader: true, completionResponse: { (response) in
                                Commands.println(object: response)
                                
                                            do
                                            {
                                               let data = response as? [String : Any]
                                               if let code = data?["code"] as? Int  {
                                                   if code == 200 {
                                                   }
                                               }
                                            }
                                            catch
                                            {
                                            }
                            }) { (error) in

                            }
                   
    }
    
}

