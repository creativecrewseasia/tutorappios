//
//  File.swift
//  DoctorApp
//
//  Created by Atinder Kaur on 8/25/20.
//  Copyright © 2020 Cerebrum Infotech. All rights reserved.
//

import Foundation

protocol MeetingDelegate: class {
   
     func getSubjectList (result : subjectData)
    func getTimeSlot (slots: Slots)
}



class AddMeetingViewModel
{
    var view : AddMeetingVC
    var  localMeetingDelegate :MeetingDelegate?
  
    init(view :AddMeetingVC, delegate:MeetingDelegate )
       {
           self.view = view
           localMeetingDelegate = delegate
       }
    
   
    
    func getSubjectList() {
        WebService.Shared.GetApi(url: APIAddress.SubjecList , Target: self.view, showLoader: true, completionResponse: { (response) in
                   
                    Commands.println(object: response)
                   
                               do
                               {
                                   let jsonData = try JSONSerialization.data(withJSONObject: response, options: .prettyPrinted)
                                   let model = try JSONDecoder().decode(subjectData.self, from: jsonData)
                                   
                                   self.localMeetingDelegate?.getSubjectList(result: model)
                                   
                   
                               }
                               catch
                               {
                                   self.view.showToastSwift(alrtType: .error, msg: kResponseNotCorrect, title: kOops)
                               }
                   
               }) { (error) in
                   self.view.showToastSwift(alrtType: .error, msg: error, title: kOops)
               }
    }
    
    func getSlots(Params : [String:Any]) {
        
        
        WebService.Shared.PostApi(url: APIAddress.TimeSlot, parameter: Params, Target: self.view, showLoader: true, completionResponse: { (response) in
            Commands.println(object: response)
            
                        do
                        {
                            let jsonData = try JSONSerialization.data(withJSONObject: response, options: .prettyPrinted)
                            let model = try JSONDecoder().decode(Slots.self, from: jsonData)
                            
                            self.localMeetingDelegate?.getTimeSlot(slots: model)
                            
            
                        }
                        catch
                        {
                            self.view.showToastSwift(alrtType: .error, msg: kResponseNotCorrect, title: kOops)
                        }
        }) { (error) in
                                      self.view.showToastSwift(alrtType: .error, msg: error, title: kOops)

        }
        
 
    }
    
    func addBooking(Params : [String:Any]) {
        
        WebService.Shared.PostApi(url: APIAddress.AddBooking, parameter: Params, Target: self.view, showLoader: true, completionResponse: { (response) in
                 Commands.println(object: response)
                 
                             do
                             {
                                let data = response as? [String : Any]
                                if let code = data?["code"] as? Int  {
                                    if code == 200 {
                                        self.view.AlertMessageWithOkAction(titleStr: kAppName, messageStr: "Booking has been added successfully.", Target: self.view)
                                                           {
                                                  self.view.navigationController?.popViewController(animated: true)
                                                           }
                                    }
                                }
                                 
                 
                             }
                             catch
                             {
                                 self.view.showToastSwift(alrtType: .error, msg: kResponseNotCorrect, title: kOops)
                             }
             }) { (error) in
                                           self.view.showToastSwift(alrtType: .error, msg: error, title: kOops)

             }
    }
    
    
}

