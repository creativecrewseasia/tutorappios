//
//  SubjectListModel.swift
//  DoctorApp
//
//  Created by Atinder Kaur on 8/25/20.
//  Copyright © 2020 Cerebrum Infotech. All rights reserved.
//

import Foundation

// MARK: - Welcome
struct subjectData: Codable {
    let code: Int
    let success: Bool
    let message: String
    let data: SubjectList
}

// MARK: - DataClass
struct SubjectList: Codable {
    let dataClass, subject: [Class]

    enum CodingKeys: String, CodingKey {
        case dataClass = "class"
        case subject
    }
}

// MARK: - Class
struct Class: Codable {
    let id, name: String
}

