//
//  DasboardDataModel.swift
//  DoctorApp
//
//  Created by Atinder Kaur on 8/25/20.
//  Copyright © 2020 Cerebrum Infotech. All rights reserved.
//

import Foundation

// MARK: - Welcome
struct ListModel : Codable {
    let code: Int?
    let success: Bool?
    let message: String?
    let data: [SubList]?
}

// MARK: - Datum
struct SubList: Codable {
    let id, bookingDate, teacherId, timeSlot, userID,subjectName, subjectId : String?
    let fName, lName: String?
      let channelName, accessToken, credit: String?
      let status: Int?
       let userDetail: UserDetail2?
    
    enum CodingKeys: String, CodingKey {
         case id, bookingDate, teacherId, fName, lName , subjectName, subjectId
         case userID = "userId"
         case timeSlot, channelName, accessToken, status, userDetail, credit
     }
}

// MARK: - UserDetail
struct UserDetail2: Codable {
    let fName, lName, dob, uniqueID: String?

    enum CodingKeys: String, CodingKey {
        case fName, lName, dob
        case uniqueID = "uniqueId"
    }
}

