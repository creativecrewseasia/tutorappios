//
//  TimeSlotDataModel.swift
//  DoctorApp
//
//  Created by Atinder Kaur on 8/25/20.
//  Copyright © 2020 Cerebrum Infotech. All rights reserved.
//


import Foundation

// MARK: - Welcome
struct Slots: Codable {
    let code: Int?
    let success: Bool?
    let message: String?
    let data: [String]?
}

