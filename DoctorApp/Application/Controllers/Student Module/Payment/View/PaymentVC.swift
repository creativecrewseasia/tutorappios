//
//  PaymentVC.swift
//  DoctorApp
//
//  Created by Atinder Kaur on 8/26/20.
//  Copyright © 2020 Cerebrum Infotech. All rights reserved.
//

import UIKit
import Stripe


class PaymentVC: CustomController {
    
    @IBOutlet weak var paymentView: UIView!
    @IBOutlet weak var btnBuy: ButtonWithShadowAndRadious!
    @IBOutlet weak var lblAmount: UILabel!
    var viewModel:PaymentModel?
    var planId = ""
    var amount = ""
    @IBOutlet weak var btnPay: ButtonWithShadowAndRadious!
    fileprivate var addCardViewController_BookingPayment = STPAddCardViewController()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnBuy.updateLayerProperties()
        btnPay.updateLayerProperties()
        self.title = "Payment"
        lblAmount.text = "Rs" + " " + amount
      self.viewModel = PaymentModel.init(view: self, delegate: self)
        
    }
    

    @IBAction func actionBuyToken(_ sender: UIButton) {
        var param = [String : Any]()
         param["userId"] = AppDefaults.shared.userID
               param["planId"] = planId
                   param["amount"] = amount
           self.viewModel?.buyToken(param: param)
    }
    
    @IBAction func actionPay(_ sender: UIButton) {
      self.addCardViewController_BookingPayment = STPAddCardViewController()
      addCardViewController_BookingPayment.delegate = self
        
        // Present add card view controller
                             let navigationController = UINavigationController(rootViewController: self.addCardViewController_BookingPayment)
                             self.present(navigationController, animated: true)
    }
    
}

extension PaymentVC : PaymentDelegate {
    func getSubjectList(result: String) {
        
    }
    
    
}

extension PaymentVC: STPAddCardViewControllerDelegate
{
    
    func addCardViewController(_ addCardViewController: STPAddCardViewController, didCreateToken token: STPToken, completion: @escaping STPErrorBlock)
    {
     
        self.addCardViewController_BookingPayment.dismiss(animated: true, completion: nil)

    }
    
    func addCardViewControllerDidCancel(_ addCardViewController: STPAddCardViewController)
    {
        
            self.addCardViewController_BookingPayment.dismiss(animated: true, completion: nil)
       
    }
}
