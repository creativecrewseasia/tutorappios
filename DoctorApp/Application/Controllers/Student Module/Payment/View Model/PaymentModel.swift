//
//  PaymentModel.swift
//  DoctorApp
//
//  Created by Atinder Kaur on 8/26/20.
//  Copyright © 2020 Cerebrum Infotech. All rights reserved.
//

import Foundation


protocol PaymentDelegate: class {
   
     func getSubjectList (result : String )
}



class PaymentModel
{
    var view : PaymentVC
    var  localPaymentDelegate :PaymentDelegate?
  
    init(view :PaymentVC, delegate:PaymentDelegate )
       {
           self.view = view
           localPaymentDelegate = delegate
       }
    
   
    
    func buyToken(param : [String : Any]) {
        
        
        WebService.Shared.PostApi(url: APIAddress.TokenAdd, parameter: param, Target: self.view, showLoader: true, completionResponse: { (response) in
            
            do
                                     {
                                        let data = response as? [String : Any]
                                        if let code = data?["code"] as? Int  {
                                            if code == 200 {
                                                self.view.AlertMessageWithOkAction(titleStr: kAppName, messageStr: "Payment has been done successfully.", Target: self.view)
                                                                   {
                                                          self.view.navigationController?.popViewController(animated: true)
                                                                   }
                                            }
                                        }
                                         
                         
                                     }
                                     catch
                                     {
                                         self.view.showToastSwift(alrtType: .error, msg: kResponseNotCorrect, title: kOops)
                                     }
            
        }) { (error) in
                               self.view.showToastSwift(alrtType: .error, msg: error, title: kOops)

        }
        
       
    }
    
    
    
}

