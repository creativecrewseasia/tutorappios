//
//  AddTokenListModel.swift
//  DoctorApp
//
//  Created by Navaldeep Kaur on 25/08/20.
//  Copyright © 2020 Cerebrum Infotech. All rights reserved.
//

import Foundation

// MARK: - AddTokenList
struct AddTokenListModel: Codable {
    let code: Int?
    let success: Bool?
    let message: String?
    let data: [Datum]?
}

// MARK: - Datum
struct Datum: Codable {
    let id, tokenCount: Int?
    let token_amount: Double?
    let description: String?

}


//struct AddTokenListModel {
//    let code: Int?
//    let success: Bool?
//    let message: String?
//    let data = [Datum]?
//
//    init(dict: [String:Any])
//           {
//            self.code = dict["code"] as? Int
//            self.message = dict["message"] as? String
//            if let arrjobList = dict["data"] as? [[String:Any]]{
//                self.body.removeAll()
//                _ = arrjobList.map({ (dict) in
//                    let model = Datum.init(id: dict["id"] as? String, tokenCount: dict["tokenCount"] as? String, tokenAmount: dict["tokenAmount"] as? Double,description:dict["description"] as? String)
//                    self.body.append(model)
//                })
//            }
//            }
//}

// MARK: - PlanList
struct PlanList {
    let id, tokenCount: Int?
    let tokenAmount: Double?
    let description: String?
    init(id:Int?,tokenCount:Int?,tokenAmount:Double?,description:String?)
       {
           self.id  = id
           self.tokenCount = tokenCount
           self.tokenAmount = tokenAmount
        self.description = description
       }

}
