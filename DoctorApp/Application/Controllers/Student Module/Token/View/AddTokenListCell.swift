//
//  AddTokenListCell.swift
//  DoctorApp
//
//  Created by Navaldeep Kaur on 25/08/20.
//  Copyright © 2020 Cerebrum Infotech. All rights reserved.
//

import UIKit
var strFromSideMenu = ""

class AddTokenListCell: UITableViewCell {
    @IBOutlet weak var btnChoose: CustomButton!
    @IBOutlet weak var lblTitle: UILabel!
    
    var viewDelegate : PlanDelegate?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
          btnChoose.backgroundColor = Appcolor.kYellowTheme
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    //MARK:- Action
    @IBAction func chooseAction(_ sender: UIButton) {
        
        btnChoose.backgroundColor = Appcolor.kSelectedYellow
        
        viewDelegate?.choosePlan(index:sender.tag)
    }
}
