//
//  AddTokenListVC.swift
//  DoctorApp
//
//  Created by Navaldeep Kaur on 25/08/20.
//  Copyright © 2020 Cerebrum Infotech. All rights reserved.
//

import UIKit

protocol PlanDelegate:class {
    func choosePlan(index:Int?)
}

class AddTokenListVC: UIViewController {
    
    //MARK:-outlet and variables
    @IBOutlet weak var tbleView: UITableView!
    
    var viewModel :AddTokenListViewModel?
    var planList = [PlanList]()
    var planArray : PlanList?
    
    //MARK:- lifeCycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setView()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        showNAV_BAR(controller: self)
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        strFromSideMenu = ""
    }
    
    //MARK:- Other Function
    func setView(){
        viewModel = AddTokenListViewModel.init(Delegate: self, view: self)
        tbleView.delegate = self
        tbleView.dataSource = self
        tbleView.tableFooterView = UIView()
        tbleView.separatorStyle = .none
        getPlanList()
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    //MARK:- Api hit
    func getPlanList(){
        viewModel?.getPlanListApi(completion: { (response) in
            if let data = response.data{
                if data.count > 0{
                    
                    for list in data{
                        self.planArray = PlanList.init(id: list.id ?? 0, tokenCount: list.tokenCount ?? 0, tokenAmount: list.token_amount ?? 0.0, description: list.description ?? "")
                        self.planList.append(self.planArray!)
                    }
                    self.planList.insert(PlanList.init(id: 7, tokenCount: 0, tokenAmount: 0.0, description: "Other"), at: self.planList.count)
                   
                    self.tbleView.setEmptyMessage("")
                    self.tbleView.reloadData()
                                    }
                else{
                    self.tbleView.setEmptyMessage("Sorry nothing to show!")
                
                }
            }
        })
    }
    
}

//MARK:- TableView Delegate
extension AddTokenListVC : UITableViewDataSource,UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return planList.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddTokenListCell", for: indexPath)as! AddTokenListCell
        
        cell.viewDelegate = self
        cell.btnChoose.tag = indexPath.row
        cell.lblTitle.text = planList[indexPath.row].description ?? ""
       
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: false)
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 93
    }
}

//MARK:- ViewDelegate

extension AddTokenListVC : PlanDelegate{
    func choosePlan(index: Int?) {
        
        if strFromSideMenu == "sideMenu" {
            if AllUtilies.isConnectedToInternet
                             {
                                let storyBoard: UIStoryboard = UIStoryboard(name: "Student2", bundle: nil)
                                let newViewController = storyBoard.instantiateViewController(withIdentifier: "PaymentVC") as! PaymentVC
                                     newViewController.modalPresentationStyle = .fullScreen
                                newViewController.planId = "\(planList[index ?? 0].id ?? 0)"
                                 newViewController.amount = "\(planList[index ?? 0].tokenAmount ?? 0.0)"
                                self.navigationController?.pushViewController(newViewController, animated: true)
                     }
                             else
                             {
                                 self.showToastSwift(alrtType:.info, msg: "No internet connection.", title:kOops)
                             }
        }
        
        else {
            SignupVC.planId = "\(planList[index ?? 0].id ?? 0)"
                SignupVC.ammount = "\(planList[index ?? 0].tokenAmount ?? 0.0)"
                
                AlertMessageWithOkAction(titleStr: kAppName, messageStr: "Plan Choosed", Target: self) {
                self.navigationController?.popViewController(animated: false)
        }
    

        }
    }
    
    
}
