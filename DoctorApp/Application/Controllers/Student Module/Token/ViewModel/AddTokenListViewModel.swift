//
//  AddTokenListViewModel.swift
//  DoctorApp
//
//  Created by Navaldeep Kaur on 25/08/20.
//  Copyright © 2020 Cerebrum Infotech. All rights reserved.
//

import Foundation
import UIKit

protocol AddTokenListDelegate:class
{
    func Show(msg: String)
    func didError(error:String)
}

class AddTokenListViewModel
{
    typealias successHandler = (AddTokenListModel) -> Void
    
    var delegate : AddTokenListDelegate
    var view : UIViewController
    var isSearching : Bool?
    var showLoader:Bool?
    
    init(Delegate : AddTokenListDelegate, view : UIViewController)
    {
        delegate = Delegate
        self.view = view
    }
    
    //MARK:- GetHomeServiceApi
    func getPlanListApi(completion: @escaping successHandler)
    {
        WebService.Shared.GetApi(url: APIAddress.planListApi, Target: self.view, showLoader: true, completionResponse: { (response) in
            do
            {
                let jsonData = try JSONSerialization.data(withJSONObject: response, options: .prettyPrinted)
                let getAllListResponse = try JSONDecoder().decode(AddTokenListModel.self, from: jsonData)
                completion(getAllListResponse)
            }
            catch
            {
                self.delegate.didError(error: kResponseNotCorrect)
                self.view.showToastSwift(alrtType: .error, msg: kResponseNotCorrect, title: kOops)
            }
        }) { (error) in
            self.delegate.didError(error: error)
            self.view.showToastSwift(alrtType: .error, msg: error, title: kFailed)
        }
        
    }
    
}

//MARK:- AddTokenListDelegate
extension AddTokenListVC : AddTokenListDelegate
{
    func Show(msg: String) {
    }
    
    func didError(error: String) {
        
           // orderHistory.removeAll()
            tbleView.setEmptyMessage("Sorry nothing to show!")
       
    }
    
    
}
