//
//  AppDelegate.swift
//  DoctorApp
//
//  Created by Mohit Sharma on 8/17/20.
//  Copyright © 2020 Cerebrum Infotech. All rights reserved.
//

import UIKit
import CoreData
import Firebase
import FirebaseAuth
import FirebaseInstanceID
import FirebaseMessaging
import Alamofire
import UserNotificationsUI
import PushKit
import Stripe



var appDelegate: AppDelegate{
    guard let delegate = UIApplication.shared.delegate as? AppDelegate else {
        fatalError("AppDelegate is not UIApplication.shared.delegate")
    }
    return delegate
}
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate, MessagingDelegate
{
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        Stripe.setDefaultPublishableKey("pk_test_51HKJUQCBB67KmVHQEaA9NvPeuA915BPNfeqBAqIW71JEeMEimhp0nVuRIaAO6la7eoeowxCRxuEAGA518ertkoYt00ifgFukXg")
        registerForPushNotifications()
        set_nav_bar_color()
        
        setRootViewController()
        
        
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            center.requestAuthorization(options: [.alert, .badge, .sound])  { (granted, error) in
                // Enable or disable features based on authorization.
                if granted {
                    DispatchQueue.main.async {
                        UIApplication.shared.registerForRemoteNotifications()}
                }
            }
        } else {
            // REGISTER FOR PUSH NOTIFICATIONS
            let notifTypes:UIUserNotificationType  = [.alert, .badge, .sound]
            let settings = UIUserNotificationSettings(types: notifTypes, categories: nil)
            application.registerUserNotificationSettings(settings)
            application.registerForRemoteNotifications()
            application.applicationIconBadgeNumber = 0
            
        }
        
        
        //Status bar color
        if #available(iOS 13.0, *)
        {
            
            
        }
        else
        {
            if let StatusbarView = UIApplication.shared.value(forKey: "statusBar") as? UIView
            {
                StatusbarView.backgroundColor = Appcolor.kTheme_Color
            }
        }
        
        
        
        //Nav bar title Bold
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font:  UIFont.boldSystemFont(ofSize: 18)]
        
        
        
        if #available(iOS 10, *)
        {
            UNUserNotificationCenter.current().delegate = self
            UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in }
            application.registerForRemoteNotifications()
        }
        else
        {
            application.registerForRemoteNotifications(matching: [.badge, .sound, .alert])
        }
        
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        return true
    }
    
    //    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
    //
    //        print("DEVICE TOKEN = \(deviceToken)")
    //        let tokenParts = deviceToken.map { data in String(format: "%02.2hhx", data)}
    //        let token = tokenParts.joined()
    //        print("Device Token: \(token)")
    //        let pasteboard = UIPasteboard.general
    //        pasteboard.string = token
    //        UserDefaults.standard.set(token, forKey: "token")
    //
    //    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print(error)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print(userInfo)
        
        if application.applicationState == .active
        {
            if self.window != nil {
                Notificationview.sharedInstance.createNotificationview(win: self.window!)
                Notificationview.sharedInstance.showNotificationView(userInfo as NSDictionary)
            }
            
        }
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        
        let dict = (response.notification.request.content.userInfo)
        
        print("dict: ",dict)
        
        
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    {
        completionHandler([.alert, .badge, .sound])
    }
    
    
    @available(iOS 13.0, *)
    
    // MARK: UISceneSession Lifecycle
    
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    
    
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    
    
    func showPermissionAlert()
    {
        let alert = UIAlertController(title: "Alert", message: "Please enable access to Notifications in the Settings app.", preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: "Settings", style: .default) {[weak self] (alertAction) in
            self?.gotoAppSettings()
        }
        
        let cancelAction = UIAlertAction(title: KCancel, style: .default, handler: nil)
        
        alert.addAction(settingsAction)
        alert.addAction(cancelAction)
        
        DispatchQueue.main.async
            {
                self.window?.rootViewController?.present(alert, animated: true, completion: nil)
        }
    }
    private func gotoAppSettings()
    {
        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else
        {
            return
        }
        
        if UIApplication.shared.canOpenURL(settingsUrl)
        {
            if #available(iOS 10.0, *)
            {
                UIApplication.shared.open(settingsUrl, options: [:]) { (success) in
                    print(success)
                }
            }
            else
            {
                
            }
        }
    }
    
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String)
    {
        print("Firebase registration token: \(fcmToken)")
        
        //let dataDict:[String: String] = ["token": fcmToken]
        // NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    
    
    
    //    //MARK:- Push Configuration
    //    func application(_ application: UIApplication,
    //                     didFailToRegisterForRemoteNotificationsWithError error: Error)
    //    {
    //        print("Failed to register: \(error)")
    //        AppDefaults.shared.userDeviceToken = "11111111111"
    //    }
    //
    func application(_ application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data)
    {
        print("Successfully registered for notifications!")
        let deviceTokenString = deviceToken.hexString
        print(deviceTokenString)
        AppDefaults.shared.userDeviceToken = deviceTokenString
        Auth.auth().setAPNSToken(deviceToken, type: AuthAPNSTokenType.unknown)
        
        
        InstanceID.instanceID().instanceID(handler: { (result, error) in
            if let error = error
            {
                print("Error fetching remote instange ID: \(error)")
                AppDefaults.shared.firebaseToken = "123"
            }
            else if let result = result
            {
                print("Remote instance ID token: \(result.token)")
                AppDefaults.shared.firebaseToken = result.token
            }
        })
    }
    //
    
    func registerForPushNotifications()
    {
        UNUserNotificationCenter.current().delegate = self
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
            (granted, error) in
            guard granted else { return }
            self.getNotificationSettings()
        }
    }
    
    func getNotificationSettings()
    {
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            print("Notification settings: \(settings)")
        }
    }
    
    
    //    func userNotificationCenter(_ center: UNUserNotificationCenter,
    //                                  willPresent notification: UNNotification,
    //        withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    //    {
    //       // let userInfo = notification.request.content.userInfo
    //
    //        completionHandler([[.alert, .sound]])
    //      }
    //
    //      func userNotificationCenter(_ center: UNUserNotificationCenter,
    //                                  didReceive response: UNNotificationResponse,
    //                                  withCompletionHandler completionHandler: @escaping () -> Void)
    //      {
    //        let userInfo = response.notification.request.content.userInfo
    //
    //        print(userInfo)
    //        
    //       
    //
    //        completionHandler()
    //      }
    //    
    
    
    
    
    func set_nav_bar_color()
    {
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        UINavigationBar.appearance().shadowImage = UIImage()
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().barTintColor = Appcolor.kTheme_Color
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().clipsToBounds = false
        UINavigationBar.appearance().backgroundColor = Appcolor.kTheme_Color
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.font : (UIFont(name: "Helvetica Neue", size: 18))!, NSAttributedString.Key.foregroundColor: UIColor.white]
        
    }
    
    func setRootViewController()
    {
        
        // self.setRootView("SWRevealViewController", storyBoard: "Home")
        
        if AppDefaults.shared.userID == "0"
        {
            self.setRootView("LoginVC", storyBoard: "Main")
        }
        else
        {
            if AppDefaults.shared.userTYPE == 2
            {
                self.setRootView("SWRevealVC", storyBoard: "Mentor")
            }
            else{
                self.setRootView("SWRevealVC", storyBoard: "Student2")
                
            }
            
        }
    }
    
}


extension Data
{
    var hexString: String
    {
        let hexString = map { String(format: "%02.2hhx", $0) }.joined()
        return hexString
    }
}

