//
//  MentorMentorDBViewModel.swift
//  DoctorApp
//
//  Created by Atinder Kaur on 8/26/20.
//  Copyright © 2020 Cerebrum Infotech. All rights reserved.
//

import Foundation
protocol MentorDBDelegate: class {
   
    func addedSuccessfully (result : ListModel)
    func callStarted (tag : Int)
    func failed()
}



class MentorMentorDBViewModel
{
    var view : MentorDashboardVC
    var localMentorDBDelegate : MentorDBDelegate?
  
    init(view : MentorDashboardVC, delegate: MentorDBDelegate )
       {
           self.view = view
           localMentorDBDelegate = delegate
       }
    
    func getHistoryClasses()
    {

        WebService.Shared.GetApi(url: APIAddress.TeacherHistoryList + AppDefaults.shared.userID, Target: self.view, showLoader: true, completionResponse: { (response) in
            
             Commands.println(object: response)
            
                        do
                        {
                            let jsonData = try JSONSerialization.data(withJSONObject: response, options: .prettyPrinted)
                            let model = try JSONDecoder().decode(ListModel.self, from: jsonData)
                            
                            self.localMentorDBDelegate?.addedSuccessfully(result: model)
                            
            
                        }
                        catch
                        {
                            self.view.showToastSwift(alrtType: .error, msg: kResponseNotCorrect, title: kOops)
                        }
            
        }) { (error) in
            self.view.showToastSwift(alrtType: .error, msg: error, title: kOops)
        }
        
    }
    
    func getActiveClasses() {
       
        WebService.Shared.GetApi(url: APIAddress.TeacherActiveList + AppDefaults.shared.userID, Target: self.view, showLoader: true, completionResponse: { (response) in
            
             Commands.println(object: response)
            
                        do
                        {
                            let jsonData = try JSONSerialization.data(withJSONObject: response, options: .prettyPrinted)
                            let model = try JSONDecoder().decode(ListModel.self, from: jsonData)
                            
                            self.localMentorDBDelegate?.addedSuccessfully(result: model)
                            
            
                        }
                        catch
                        {
                            self.view.showToastSwift(alrtType: .error, msg: kResponseNotCorrect, title: kOops)
                        }
            
        }) { (error) in
            self.view.showToastSwift(alrtType: .error, msg: error, title: kOops)
        }
    }
    

    func startVideoCall(Params : [String:Any], tag : Int) {
      
            
            WebService.Shared.PostApi(url: APIAddress.StartVideoCall, parameter: Params, Target: self.view, showLoader: true, completionResponse: { (response) in
                     Commands.println(object: response)
                     
                                 do
                                 {
                                    let data = response as? [String : Any]
                                    if let code = data?["code"] as? Int  {
                                        if code == 200 {
                                            self.localMentorDBDelegate?.callStarted(tag: tag)
                                        }
                                    }
                                     
                     
                                 }
                                 catch
                                 {
                                     self.view.showToastSwift(alrtType: .error, msg: kResponseNotCorrect, title: kOops)
                                 }
                 }) { (error) in
                                               self.view.showToastSwift(alrtType: .error, msg: error, title: kOops)

                 }
        
    }
    
    func endVideoCall(Params : [String:Any]) {
          
                
                WebService.Shared.PostApi(url: APIAddress.CompleteVideoCall, parameter: Params, Target: self.view, showLoader: true, completionResponse: { (response) in
                         Commands.println(object: response)
                         
                                     do
                                     {
                                        let data = response as? [String : Any]
                                        if let code = data?["code"] as? Int  {
                                            if code == 200 {
//                                                 self.view.navigationController?.popViewController(animated: true)
                                                let storyBoard: UIStoryboard = UIStoryboard(name: "Mentor", bundle: nil)
                                                let newViewController = storyBoard.instantiateViewController(withIdentifier: "FeedBackToStudent") as! FeedBackToStudent
                                                newViewController.modalPresentationStyle = .fullScreen
                                                self.view.navigationController?.pushViewController(newViewController, animated: true)
                                            }
                                        }
                                     }
                                     catch
                                     {
                                         self.view.showToastSwift(alrtType: .error, msg: kResponseNotCorrect, title: kOops)
                                     }
                     }) { (error) in
                                                   self.view.showToastSwift(alrtType: .error, msg: error, title: kOops)

                     }
            
        }
    
    func updateDeviceToken(Params : [String:Any]) {
        WebService.Shared.PostApi(url: APIAddress.DeviceToken, parameter: Params, Target: self.view, showLoader: true, completionResponse: { (response) in
                                Commands.println(object: response)
                                            do
                                            {
                                               let data = response as? [String : Any]
                                               if let code = data?["code"] as? Int  {
                                                   if code == 200 {
                                                   }
                                               }
                                            }
                                            catch
                                            {
                                            }
                            }) { (error) in

                            }
                   
    }

}

